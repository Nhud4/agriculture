import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
    },
    colors: {
      white: "#FFFF",
      black: {
        80: "#444444",
        DEFAULT: "#000000",
      },
      primary: {
        2: "#718EBF",
        3: "#396AFF",
        4: "#E7EDFF",
        5: "#4E95B2",
        DEFAULT: "#3A5E8A",
      },
      yellow: {
        2: "#FFF5D9",
        DEFAULT: "#FFBB38",
      },
      pink: {
        2: "#FFE0EB",
        DEFAULT: "#FF82AC",
      },
      cyn: {
        2: "#DCFAF8",
        DEFAULT: "#16DBCC",
      },
      danger: {
        50: "#FBEAE9",
        500: "#D62A24",
        DEFAULT: "#F26464",
      },
      neutral: {
        2: "#DFEAF2",
        3: "#cccccc",
        DEFAULT: "#E5E5E5",
      },
      gray: {
        3: "#828282",
        60: "#B5B5B5",
        70: "#F5F6FA",
        90: "#282A3A",
      },
      green: {
        2: "#EAF7EA",
        DEFAULT: "#2CB22E",
      },
    },
  },
  plugins: [],
};
export default config;
