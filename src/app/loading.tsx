"use client";

import React from "react";
import Image from "next/image";
import IMAGES from "@/config/images";

export default function Loading() {
  return (
    <div className="flex flex-col justify-center items-center bg-white h-screen">
      <Image src={IMAGES.LoadingData} alt="image" width={550} />
      <h1 className="text-2xl font-semibold">Loading...</h1>
    </div>
  );
}
