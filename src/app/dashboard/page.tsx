"use client";

import React, { useEffect, useState } from "react";
import CustomLayout from "@/components/layout/CustomLayout";
import SummaryCard from "@/components/modules/summaryCard";
import BarChart from "@/components/modules/barChart";
import PieChart from "@/components/modules/pieChart";
import LineChart from "@/components/modules/lineChart";
import { chartData, pirChartData } from "@/utils/dummies";
import DashboardComponent from "@/components/features/dashboard";

export default function Dashboard() {
  const [token, setToken] = useState<string | null>("");
  useEffect(() => {
    if (typeof window !== undefined) {
      const token = localStorage.getItem("token") || null;
      setToken(token || "");
    }
  }, []);

  return (
    <CustomLayout>
      <section>
        <DashboardComponent token={token as string} />
      </section>
    </CustomLayout>
  );
}
