"use client";

import React, { useState, useEffect } from "react";
import Image from "next/image";
import IMAGES from "@/config/images";
import { useRouter } from "next/navigation";

export default function Loading() {
  const [token, setToken] = useState<string | null>("");
  const router = useRouter();

  useEffect(() => {
    if (typeof window !== undefined) {
      const token = localStorage.getItem("token") || null;
      setToken(token || "");
    }
  }, []);

  useEffect(() => {
    if (token) {
      router.replace("/dashboard");
    } else {
      router.replace("/login");
    }
  }, [token]);

  return (
    <div className="flex flex-col justify-center items-center bg-white h-screen">
      <Image src={IMAGES.LoadingData} alt="image" width={550} />
      <h1 className="text-2xl font-semibold">Loading...</h1>
    </div>
  );
}
