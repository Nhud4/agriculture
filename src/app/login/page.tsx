"use client";

import React from "react";
import IMAGES from "@/config/images";
import Image from "next/image";
import LoginForm from "@/components/elements/form/login";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function Login() {
  return (
    <section>
      <div className="grid grid-cols-2 h-screen">
        <div className="flex justify-center items-center">
          <Image alt="logo" src={IMAGES.FullLogo} width={400} priority />
        </div>
        <div className="flex justify-center items-center bg-primary">
          <LoginForm />
        </div>
      </div>
      <ToastContainer pauseOnFocusLoss={false} pauseOnHover={false} />
    </section>
  );
}
