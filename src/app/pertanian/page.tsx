"use client";

import React, { useEffect, useState } from "react";
import CustomLayout from "@/components/layout/CustomLayout";
import TablePertanian from "@/components/features/pertanian/TablePertanian";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function Pertanian() {
  const [token, setToken] = useState("");
  useEffect(() => {
    if (typeof window !== undefined) {
      const token = localStorage.getItem("token") || null;
      setToken(token || "");
    }
  }, []);
  return (
    <CustomLayout>
      <section className="page">
        <TablePertanian token={token} />
        <ToastContainer pauseOnFocusLoss={false} pauseOnHover={false} />
      </section>
    </CustomLayout>
  );
}
