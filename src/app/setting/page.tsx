"use client";

import React, { useEffect, useState } from "react";
import CustomLayout from "@/components/layout/CustomLayout";
import PusatData from "@/components/features/puastData";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function Settings() {
  const [token, setToken] = useState("");
  useEffect(() => {
    if (typeof window !== undefined) {
      const token = localStorage.getItem("token") || null;
      setToken(token || "");
    }
  }, []);
  return (
    <CustomLayout>
      <section className="page">
        <PusatData token={token} />
        <ToastContainer pauseOnFocusLoss={false} pauseOnHover={false} />
      </section>
    </CustomLayout>
  );
}
