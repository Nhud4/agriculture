"use server";

import * as req from "@/utils/httpRequest";

const endpoint = {
  main: "/plants",
};

export const fetchListPlants = async (token: string, params?: PlansParams) => {
  const { data } = await req.get<ApiResponse<PlansList[]>>(
    token,
    `${endpoint.main}/list`,
    params
  );

  const newData = data.map((item, index) => ({
    ...item,
    no: index + 1,
  }));

  return newData;
};

export const fetchDetailPlants = async (token: string, code: string) => {
  const { data } = await req.get<ApiResponse<PlantsDetail>>(
    token,
    `${endpoint.main}/detail/${code}`
  );
  return data;
};

export const fetchAddPlants = async (token: string, payload: PLansPayload) => {
  const data = await req.post(token, `${endpoint.main}/add`, payload);
  return data;
};

export const fetchEditPlants = async (
  token: string,
  code: number,
  payload: PLansPayload
) => {
  const data = await req.put(token, `${endpoint.main}/edit/${code}`, payload);
  return data;
};

export const fetchDeletePlants = async (token: string, code: number) => {
  const data = await req.remove(token, `${endpoint.main}/delete/${code}`);
  return data;
};
