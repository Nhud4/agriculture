type PlansList = {
  no: number;
  id: number;
  code: string;
  jenis: string;
  active: boolean;
};

type PlansParams = {
  search?: string;
};

type PlantsDetail = {
  id: number;
  code: string;
  active: boolean;
  jenis: string;
};

type PLansPayload = {
  jenis: string;
};
