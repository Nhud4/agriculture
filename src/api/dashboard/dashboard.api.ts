"use server";

import * as req from "@/utils/httpRequest";

const endpoint = {
  main: "/dashboard",
};

export const fetchSummary = async (token: string) => {
  const { data } = await req.get<ApiResponse<SummaryList>>(
    token,
    `${endpoint.main}/summary`
  );
  return data;
};

export const fetchGrafikTanaman = async (token: string) => {
  const { data } = await req.get<ApiResponse<GrafikTanaman[]>>(
    token,
    `${endpoint.main}/tanaman`
  );
  return data;
};

export const fetchGrafikKecamatan = async (token: string) => {
  const { data } = await req.get<ApiResponse<GrafikKecamatan[]>>(
    token,
    `${endpoint.main}/kecamatan`
  );
  return data;
};

export const fetchGrafikCluster = async (token: string) => {
  const { data } = await req.get<ApiResponse<GrafikCluster[]>>(
    token,
    `${endpoint.main}/cluster`
  );
  return data;
};
