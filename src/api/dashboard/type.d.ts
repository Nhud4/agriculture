type SummaryList = {
  pertanian: number;
  kecamatan: number;
  tanaman: number;
  laporan: number;
};

type GrafikTanaman = {
  tanaman: string;
  tTanam: number;
  tPanen: number;
  tProduksi: number;
};

type GrafikCluster = {
  count: number;
  code: string;
  cluster: string;
};

type GrafikKecamatan = {
  kecamatan: string;
  tTanam: number;
  tPanen: number;
  tProduksi: number;
};
