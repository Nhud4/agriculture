"use server";

import * as req from "@/utils/httpRequest";

const endpoint = {
  main: "/agriculture",
};

export const fetchListPertanian = async (
  token: string,
  params?: PertanianParams
) => {
  const { data } = await req.get<ApiResponse<PertanianList[]>>(
    token,
    `${endpoint.main}/list`,
    params
  );

  const newData = data.map((item, index) => ({
    ...item,
    no: index + 1,
  }));

  return newData;
};

export const fetchDetailPertanian = async (token: string, code: number) => {
  const { data } = await req.get<ApiResponse<PertanianDetail>>(
    token,
    `${endpoint.main}/detail/${code}`
  );
  return data;
};

export const fetchAddPertanian = async (
  token: string,
  payload: PertanianPayload
) => {
  const data = await req.post(token, `${endpoint.main}/add`, payload);
  return data;
};

export const fetchEditPertanian = async (
  token: string,
  code: number,
  payload: PertanianPayload
) => {
  const data = await req.put(token, `${endpoint.main}/edit/${code}`, payload);
  return data;
};

export const fetchDeletePertanian = async (token: string, code: number) => {
  const data = await req.remove(token, `${endpoint.main}/delete/${code}`);
  return data;
};
