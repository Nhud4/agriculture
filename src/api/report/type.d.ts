type PertanianParams = {
  clusterId?: string;
  tanamanId?: string;
};

type PertanianList = {
  no: number;
  id: number;
  tahun: string;
  kecamatan: string;
  jenis: string;
  tanam: number;
  panen: number;
  produksi: number;
  provitas: string;
  cluster: string;
  code: string;
};

type PertanianDetail = {
  id: number;
  tahun: string;
  kecId: number;
  tanamanId: number;
  tanam: number;
  panen: number;
  produksi: number;
  provitas: string;
  clusterId: number;
  createdBy: number;
};

type PertanianPayload = {
  tahun: string;
  kecId: number;
  tanamanId: number;
  tanam: number;
  panen: number;
  produksi: number;
  provitas: string;
};
