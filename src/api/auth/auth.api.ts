"use server";

import * as req from "@/utils/httpRequest";

const endpoint = {
  main: "/auth",
};

export const fetchLogin = async (payload: LoginPayload) => {
  const { data } = await req.basicPost<ApiResponse<Login>>(
    `${endpoint.main}/login`,
    payload
  );
  console.log(data.accessToken);
  return data;
};

export const fetchRegister = async (payload: Register) => {
  const data = await req.basicPost(`${endpoint.main}/register`, payload);
  return data;
};

export const fetchListUser = async (token: string) => {
  const { data } = await req.get<ApiResponse<ListUser[]>>(
    token,
    `${endpoint.main}/list`
  );
  return data;
};

export const fetchDetailUser = async (token: string, id: number) => {
  const { data } = await req.get<ApiResponse<DetailUser>>(
    token,
    `${endpoint.main}/detail/${id}`
  );
  return data;
};

export const fetchProfile = async (token: string) => {
  const { data } = await req.get<ApiResponse<Profile>>(
    token,
    `${endpoint.main}/profile`
  );
  return data;
};

export const fetchUpdateUser = async (
  token: string,
  code: number,
  payload: UserPayload
) => {
  const data = await req.put(token, `${endpoint.main}/update/${code}`, payload);
  return data;
};

export const fetchRemoveUser = async (token: string, code: number) => {
  const response = await req.basicRemove(
    token,
    `${endpoint.main}/delete/${code}`
  );
  return response;
};
