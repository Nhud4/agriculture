type Login = {
  accessToken: string;
  expAccessToken: string;
};

type LoginPayload = {
  username: string;
  password: string;
};

type ListUser = {
  id: number;
  nama: string;
  username: string;
};

type UserPayload = {
  username: string;
  name: string;
  oldPassword: string;
  newPassword: string;
};

type Profile = {
  id: number;
  nama: string;
  username: string;
  userCategory: string;
};

type DetailUser = {
  id: number;
  name: string;
  username: string;
  userCategory: string;
  password: string;
};

type Register = {
  name: string;
  username: string;
  password: string;
  userCategory: number;
};
