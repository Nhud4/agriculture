type RegionLis = {
  no: number;
  id: number;
  code: string;
  region: string;
  active: boolean;
};

type RegionParams = {
  search?: string;
};

type RegionPayload = {
  region: string;
};

type RegionDetail = {
  id: number;
  code: string;
  region: string;
  active: boolean;
};
