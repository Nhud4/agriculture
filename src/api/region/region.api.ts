"use server";

import * as req from "@/utils/httpRequest";

const endpoint = {
  main: "/region",
};

export const fetchListRegion = async (token: string, params?: RegionParams) => {
  const { data } = await req.get<ApiResponse<RegionLis[]>>(
    token,
    `${endpoint.main}/list`,
    params
  );

  const newData = data.map((item, index) => ({
    ...item,
    no: index + 1,
  }));

  return newData;
};

export const fetchAddRegion = async (token: string, payload: RegionPayload) => {
  const data = await req.post(token, `${endpoint.main}/add`, payload);
  return data;
};

export const fetchEditRegion = async (
  token: string,
  code: number,
  payload: RegionPayload
) => {
  const data = await req.put(token, `${endpoint.main}/update/${code}`, payload);
  return data;
};

export const fetchDeleteRegion = async (token: string, code: number) => {
  const data = await req.remove(token, `${endpoint.main}/delete/${code}`);
  return data;
};
