type CLusterList = {
  no: number;
  id: number;
  code: string;
  cluster: string;
  total: number;
  active: boolean;
};

type ClusterDetail = {
  id: number;
  code: string;
  cluster: string;
  active: boolean;
};

type CLusterPayload = {
  cluster: string;
};

type ClusterCountParams = {
  tanamanId: number;
  iterasi1: {
    c1: number;
    c2: number;
    c3: number;
  };
  iterasi2: {
    c1: number;
    c2: number;
    c3: number;
  };
};
