"use server";

import * as req from "@/utils/httpRequest";

const endpoint = {
  main: "/cluster",
};

export const fetchListCluster = async (token: string) => {
  const { data } = await req.get<ApiResponse<CLusterList[]>>(
    token,
    `${endpoint.main}/list`
  );

  const newData = data.map((item, index) => ({
    ...item,
    no: index + 1,
  }));

  return newData;
};

export const fetchDetailCluster = async (token: string, code: number) => {
  const { data } = await req.get<ApiResponse<ClusterDetail>>(
    token,
    `${endpoint.main}/detail/${code}`
  );
  return data;
};

export const fetchAddCluster = async (
  token: string,
  payload: CLusterPayload
) => {
  const data = await req.post(token, `${endpoint.main}/add`, payload);
  return data;
};

export const fetchEditCluster = async (
  token: string,
  code: number,
  payload: CLusterPayload
) => {
  const data = await req.put(token, `${endpoint.main}/edit/${code}`, payload);
  return data;
};

export const fetchDeleteCluster = async (token: string, code: number) => {
  const data = await req.remove(token, `${endpoint.main}/delete/${code}`);
  return data;
};

export const fetchCountCluster = async (body: ClusterCountParams) => {
  const data = await req.basicPost(`${endpoint.main}/count`, body);
  return data;
};
