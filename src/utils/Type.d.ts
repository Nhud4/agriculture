type UserToken = {
  exp: string;
  token: string;
};

type Pertanian = {
  id: number;
  tahun: string;
  kecamatan: string;
  tanamanId: string;
  tanam: number;
  panen: number;
  produksi: number;
  provitas: number;
};

type Modal = {
  content: React.ReactNode;
  onConfirm?: () => void;
  open: boolean;
  title?: string;
};

type ApiResponse<T> = {
  code: number;
  data: T;
  message: string;
  meta?: Meta;
  success: boolean;
};

type LocalStorage = {
  token?: string;
};

type Actions = "detail" | "edit" | "delete" | "status";

type TokenComponent = {
  token: string;
};
