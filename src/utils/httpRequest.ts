"use server";

const SERVER_HOST = "http://127.0.0.1:4000";

const basicCredentials = Buffer.from(
  "agriculture:c2VydmljZWFncmljdWx0dXJl",
  "utf-8"
).toString("base64");
const timeoutDuration = 30000;

export const baseFetch = async (
  token: string,
  endpoint: string,
  method: "POST" | "PUT" | "GET" | "DELETE" | "PATCH",
  body?: Record<string, unknown>
) => {
  const url = `${SERVER_HOST}${endpoint}`;
  const headers = new Headers();
  const controller = new AbortController();
  const timeoutId = setTimeout(() => controller.abort(), timeoutDuration);
  const isFormData = body instanceof FormData;

  headers.set("Content-Type", "application/json");
  headers.set("Authorization", `Bearer ${token}`);

  return fetch(url, {
    method,
    headers,
    signal: controller.signal,
    body: isFormData ? body : JSON.stringify(body),
  }).then((response) => {
    clearTimeout(timeoutId);
    return response;
  });
};

export const basicFetch = async (
  endpoint: string,
  method: "POST" | "PUT" | "GET" | "DELETE" | "PATCH",
  body?: Record<string, unknown>
) => {
  const url = `${SERVER_HOST}${endpoint}`;
  const headers = new Headers();
  const controller = new AbortController();
  const timeoutId = setTimeout(() => controller.abort(), timeoutDuration);

  headers.set("Content-Type", "application/json");
  headers.set("Authorization", `Basic ${basicCredentials}`);

  return fetch(url, {
    method,
    headers,
    signal: controller.signal,
    body: JSON.stringify(body),
  }).then((response) => {
    clearTimeout(timeoutId);
    return response;
  });
};

export async function post<T>(
  token: string,
  endpoint: string,
  body: Record<string, unknown>
) {
  const response = await baseFetch(token, endpoint, "POST", body);
  const data = await response.json();
  if (response.status >= 400) throw data as T;
  return data as T;
}

export async function put<T>(
  token: string,
  endpoint: string,
  body: Record<string, unknown>
) {
  const response = await baseFetch(token, endpoint, "PUT", body);
  const data = await response.json();
  if (response.status >= 400) throw data as T;
  return data as T;
}

export async function patch<T>(
  token: string,
  endpoint: string,
  body: Record<string, unknown>
) {
  const response = await baseFetch(token, endpoint, "PATCH", body);
  const data = await response.json();
  if (response.status >= 400) throw data as T;
  return data as T;
}

export async function remove<T>(
  token: string,
  endpoint: string,
  body?: Record<string, unknown>
) {
  const response = await baseFetch(token, endpoint, "DELETE", body);
  const data = await response.json();
  if (response.status >= 400) throw data as T;
  return data as T;
}

export async function get<T>(
  token: string,
  endpoint: string,
  params?: Record<string, unknown>
) {
  const query: string = params
    ? Object.keys(params)
        .map((key) => `${key}=${params[key]}`)
        .join("&")
    : "";
  const response = await baseFetch(token, `${endpoint}?${query}`, "GET");
  const data = await response.json();
  if (response.status >= 400) throw data as T;
  return data as T;
}

export async function basicPost<T>(
  endpoint: string,
  body: Record<string, unknown>
) {
  const response = await basicFetch(endpoint, "POST", body);
  const data = await response.json();
  if (response.status >= 400) throw data as T;
  return data as T;
}

export async function basicRemove<T>(
  token: string,
  endpoint: string,
  body?: Record<string, unknown>
) {
  const response = await basicFetch(endpoint, "DELETE");
  const data = await response.json();
  if (response.status >= 400) throw data as T;
  return data as T;
}
