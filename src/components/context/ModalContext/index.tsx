"use client";

import React, { createContext, useEffect, useMemo, useState } from "react";
import Modal from "@/components/modules/modal";

const defaultValue: Modal = {
  content: null,
  open: false,
};
export const ModalContext = createContext<{
  modal: Modal;
  onClose: () => void;
  setModal: React.Dispatch<React.SetStateAction<Modal>>;
}>({
  modal: defaultValue,
  setModal: () => {},
  onClose: () => {},
});

type Props = {
  children: React.ReactNode;
};

export const ModalContextProvider: React.FC<Props> = ({ children }) => {
  const [modal, setModal] = useState(defaultValue);
  const onClose = () => setModal(defaultValue);
  const initialValue = useMemo(() => ({ modal, setModal, onClose }), [modal]);

  useEffect(() => {
    if (modal.open) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "auto";
    }
  }, [modal.open]);

  return (
    <ModalContext.Provider value={initialValue}>
      {children}
      <Modal {...modal} onClose={onClose}>
        {modal.content}
      </Modal>
    </ModalContext.Provider>
  );
};
