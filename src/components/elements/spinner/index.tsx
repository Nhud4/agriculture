"use client";

import React from "react";
import styles from "./styles.module.css";

export default function Spinner() {
  return (
    <div className="flex justify-center items-center w-full">
      <div
        className={[styles.spinner, "animate-spin h-5 w-5 rounded-full"].join(
          " "
        )}
      />
    </div>
  );
}
