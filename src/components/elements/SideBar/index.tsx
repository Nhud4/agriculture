"use client";

import React, { useState } from "react";
import Image from "next/image";
import IMAGES from "@/config/images";
import ROUTES from "@/config/routes";
import { usePathname } from "next/navigation";
import Link from "next/link";
import { useRouter } from "next/navigation";

const Sidebar: React.FC = () => {
  const menu = ROUTES;
  const pathname = usePathname();
  const [isHovering, setIsHovered] = useState(false);
  const [activeIndex, setActiveIndex] = useState<number | null>(null);
  const router = useRouter();

  const handleLogout = () => {
    router.push("/login");
    localStorage.clear();
  };

  return (
    <div className="fixed z-50 h-screen bg-primary text-black w-60">
      <div className="flex items-center space-x-2 px-3 py-4 border-b border-white">
        <Image alt="logo" src={IMAGES.Logo} width={40} height={40} />
        <div>
          <h1 className="text-lg text-white font-bold">Cluster Pertanian</h1>
          <h1 className="text-sm text-white">Dashboard</h1>
        </div>
      </div>
      <ul>
        {menu.map((item, index) => {
          const active = pathname === item.path;
          const hover = isHovering && activeIndex === index;
          return (
            <li key={item.path}>
              <Link
                href={item.path}
                className={[
                  "flex justify-between items-center p-4 cursor-pointer hover:bg-white",
                  active ? "bg-white" : "",
                ].join(" ")}
                onMouseEnter={() => {
                  setIsHovered(true);
                  setActiveIndex(index);
                }}
                onMouseLeave={() => {
                  setIsHovered(false);
                  setActiveIndex(null);
                }}
              >
                <div className="flex items-center space-x-4 ">
                  {active || hover ? item.activeIcon : item.icon}
                  <p
                    className={[
                      "text-lg",
                      active || hover ? "text-primary" : "text-white",
                    ].join(" ")}
                  >
                    {item.label}
                  </p>
                </div>
                <div className="w-1 h-6 bg-primary rounded-t-full rounded-b-full" />
              </Link>
            </li>
          );
        })}
      </ul>
      <div className="absolute bottom-0 w-full">
        <div className="flex justify-center pb-4">
          <button
            className="bg-primary-5 w-48 py-2 rounded-lg text-white text-center"
            onClick={handleLogout}
          >
            Keluar
          </button>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
