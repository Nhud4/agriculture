"use client";

import React, { useMemo, useState } from "react";
import InputText from "@/components/modules/inputText";
import { useQuery } from "@tanstack/react-query";
import { fetchDetailUser } from "@/api/auth/auth.api";

const defaultValues: DetailUser = {
  id: 0,
  name: "",
  username: "",
  userCategory: "",
  password: "",
};

type Props = {
  token: string;
  id: number;
};

export default function DetailUser({ token, id }: Props) {
  const { data } = useQuery({
    queryKey: ["user/detail", [id, token]],
    queryFn: async () => fetchDetailUser(token, id),
  });

  console.log(data);

  const formValues = useMemo(() => {
    if (data) {
      return { ...data, userCategory: "Administrator" };
    }

    return defaultValues;
  }, [data]);

  return (
    <div className="grid grid-cols-2 gap-4">
      <InputText
        label="ID"
        placeholder="ID"
        type="text"
        value={`${formValues.id}`}
        isDisabled
      />
      <InputText
        label="Nama"
        placeholder="Nama"
        type="text"
        value={formValues.name}
        isDisabled
      />
      <InputText
        label="Username"
        placeholder="Username"
        type="text"
        value={formValues.username}
        isDisabled
      />
      <InputText
        label="Kategori Pengguna"
        placeholder="Kategori Pengguna"
        type="text"
        value={formValues.userCategory}
        isDisabled
      />
      <InputText
        label="Kata Sandi"
        placeholder="Kata Sandi"
        type="password"
        value={formValues.password}
        isDisabled
      />
    </div>
  );
}
