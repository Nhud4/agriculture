"use client";

import React, { useContext, useState } from "react";
import InputText from "@/components/modules/inputText";
import Button from "@/components/modules/button";
import { ModalContext } from "@/components/context/ModalContext";
import { useMutation } from "@tanstack/react-query";
import Spinner from "@/components/elements/spinner";
import { fetchRegister } from "@/api/auth/auth.api";
import "react-toastify/dist/ReactToastify.css";

type Props = {
  handleSuccess: () => void;
  handleError: () => void;
};

export default function AddUserForm({ handleSuccess, handleError }: Props) {
  const { onClose } = useContext(ModalContext);
  const [error, setError] = useState("");
  const [formValues, setFormValues] = useState({
    name: "",
    username: "",
    password: "",
    userCategory: 1,
  });

  const { mutate, isPending } = useMutation({
    mutationKey: ["user/add"],
    mutationFn: (body: Register) => {
      return fetchRegister(body);
    },
    onSuccess: handleSuccess,
    onError: handleError,
  });

  const onSubmit = (e: { preventDefault: () => void }) => {
    e.preventDefault();
    if (!formValues.name || !formValues.password || !formValues.username) {
      setError("Data tidak lengkap");
    } else {
      mutate(formValues);
    }
  };

  const isDisabled = Boolean(error);

  return (
    <form className="space-y-4" onSubmit={onSubmit}>
      <InputText
        label="Nama"
        placeholder="Nama"
        type="text"
        value={formValues.name}
        className="border border-neutral"
        onChange={(value) =>
          setFormValues({ ...formValues, name: value as string })
        }
      />
      <InputText
        label="Username"
        placeholder="Username"
        type="text"
        value={formValues.username}
        className="border border-neutral"
        onChange={(value) =>
          setFormValues({ ...formValues, username: value as string })
        }
      />
      <InputText
        label="Password"
        placeholder="Password"
        type="password"
        value={formValues.password}
        className="border border-neutral"
        onChange={(value) =>
          setFormValues({ ...formValues, password: value as string })
        }
      />
      {error ? <p className="text-danger-500 pt-2">{error}</p> : null}
      <div className="flex justify-end items-center space-x-4">
        <Button
          type="button"
          className="!bg-white border border-primary !text-primary !w-32"
          onClick={onClose}
        >
          Batal
        </Button>
        <Button
          type="submit"
          isDisabled={isDisabled}
          className={[
            "!w-32",
            isDisabled ? "cursor-not-allowed" : "cursor-pointer",
          ].join(" ")}
        >
          {isPending ? <Spinner /> : "Konfirmasi"}
        </Button>
      </div>
    </form>
  );
}
