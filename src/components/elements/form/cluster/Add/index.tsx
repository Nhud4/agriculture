"use client";

import React, { useContext, useState } from "react";
import InputText from "@/components/modules/inputText";
import Button from "@/components/modules/button";
import { useMutation } from "@tanstack/react-query";
import { fetchAddCluster } from "@/api/cluster/cluster.api";
import Spinner from "@/components/elements/spinner";
import { ModalContext } from "@/components/context/ModalContext";

type Props = {
  token: string;
  handleSuccess: () => void;
  handleError: () => void;
};

const defaultValues = {
  cluster: "",
};

export default function FormAddCluster({
  token,
  handleSuccess,
  handleError,
}: Props) {
  const { onClose } = useContext(ModalContext);
  const [formValue, setFormValue] = useState(defaultValues);
  const [error, setError] = useState("");

  const { mutate, isPending } = useMutation({
    mutationKey: ["cluster/add"],
    mutationFn: (body: CLusterPayload) => {
      return fetchAddCluster(token, body);
    },
    onSuccess: handleSuccess,
    onError: handleError,
  });

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (!formValue.cluster) {
      setError("Harap melengkapi data");
    } else {
      mutate(formValue);
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <div className="space-y-2">
        <InputText
          label="Cluster"
          placeholder="Cluster"
          type="text"
          className="border border-neutral-3"
          value={formValue.cluster}
          onChange={(value) =>
            setFormValue({ ...formValue, cluster: value as string })
          }
        />
      </div>
      {error ? <p className="text-danger-500 pt-2">{error}</p> : null}
      <div className="flex justify-end items-center space-x-4 pt-4">
        <Button
          type="button"
          className="!bg-white border border-primary !text-primary !w-32"
          onClick={onClose}
        >
          Batal
        </Button>
        <Button type="submit" className="!w-32">
          {isPending ? <Spinner /> : "Konfirmasi"}
        </Button>
      </div>
    </form>
  );
}
