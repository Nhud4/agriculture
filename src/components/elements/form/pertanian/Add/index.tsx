"use client";

import React, { useContext, useEffect, useState } from "react";
import Button from "@/components/modules/button";
import { ModalContext } from "@/components/context/ModalContext";
import InputText from "@/components/modules/inputText";
import Dropdown from "@/components/modules/dropdown";
import { useQuery, useMutation } from "@tanstack/react-query";
import { fetchListPlants } from "@/api/plants/plants.api";
import { fetchListRegion } from "@/api/region/region.api";
import { fetchAddPertanian } from "@/api/report/report.api";

const defaultValues = {
  tahun: "",
  kecId: "",
  tanamanId: "",
  tanam: "",
  panen: "",
  produksi: "",
  provitas: "",
};

type Props = {
  token: string;
  onSuccess: () => void;
  onError: () => void;
};

export default function AddPertanian({ token, onSuccess, onError }: Props) {
  const { onClose } = useContext(ModalContext);
  const [formValues, setFormValues] = useState(defaultValues);
  const [error, setError] = useState("");

  const { data: plants } = useQuery({
    queryKey: ["plants/ops", [token]],
    queryFn: async () => fetchListPlants(token),
  });

  const { data: region } = useQuery({
    queryKey: ["region/ops", [token]],
    queryFn: async () => fetchListRegion(token),
  });

  const plantOps = plants?.map((item) => ({
    label: item.jenis,
    value: item.id,
  }));

  const regionOps = region?.map((item) => ({
    label: item.region,
    value: item.id,
  }));

  const { mutate } = useMutation({
    mutationKey: ["pertanian/add"],
    mutationFn: (body: PertanianPayload) => {
      return fetchAddPertanian(token, body);
    },
    onSuccess: onSuccess,
    onError: onError,
  });

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    const obj = Object.keys(formValues);
    const validate = obj.filter((key) => {
      return formValues[key as keyof typeof formValues];
    });
    console.log(validate, obj);
    if (validate.length === 0 || obj.length !== validate.length) {
      setError("Harap melengkapi data yang dibutuhkan");
    } else {
      const payload = {
        ...formValues,
        kecId: parseInt(formValues.kecId, 10) || 0,
        tanamanId: parseInt(formValues.tanamanId, 10) || 0,
        tanam: parseInt(formValues.tanam, 10) || 0,
        panen: parseInt(formValues.panen, 10) || 0,
        produksi: parseInt(formValues.produksi, 10) || 0,
      };
      mutate(payload);
    }
  };

  useEffect(() => {
    setError("");
  }, [formValues]);

  return (
    <form onSubmit={onSubmit}>
      <div className="grid grid-cols-2 gap-4">
        <InputText
          label="Tahun"
          placeholder="Tahun"
          type="text"
          className="border border-neutral-3"
          value={formValues.tahun}
          onChange={(value) =>
            setFormValues({ ...formValues, tahun: `${value}` })
          }
        />
        <Dropdown
          label="Kecamatan"
          name="region"
          options={regionOps || []}
          placeholder="Kecamatan"
          onChange={(ops) =>
            setFormValues({ ...formValues, kecId: ops?.value as string })
          }
        />
        <Dropdown
          label="Jenis Tanaman"
          name="plants"
          options={plantOps || []}
          placeholder="Tanaman"
          onChange={(ops) =>
            setFormValues({ ...formValues, tanamanId: ops?.value as string })
          }
        />
        <InputText
          label="Tanam"
          placeholder="Tanam"
          type="text"
          className="border border-neutral-3"
          value={`${formValues.tanam}`}
          onChange={(value) =>
            setFormValues({ ...formValues, tanam: value as string })
          }
          onlyNumber
        />
        <InputText
          label="Panen"
          placeholder="Panen"
          type="text"
          className="border border-neutral-3"
          value={`${formValues.panen}`}
          onChange={(value) =>
            setFormValues({ ...formValues, panen: value as string })
          }
          onlyNumber
        />
        <InputText
          label="Produksi"
          placeholder="Produksi"
          type="text"
          className="border border-neutral-3"
          value={`${formValues.produksi}`}
          onChange={(value) =>
            setFormValues({ ...formValues, produksi: value as string })
          }
          onlyNumber
        />
        <InputText
          label="Provitas"
          placeholder="Provitas"
          type="text"
          className="border border-neutral-3"
          value={formValues.provitas}
          onChange={(value) =>
            setFormValues({ ...formValues, provitas: value as string })
          }
          onlyNumber
        />
      </div>
      {error ? <p className="text-danger-500 pt-2">{error}</p> : null}
      <div className="flex justify-end items-center space-x-4">
        <Button
          type="button"
          className="!bg-white border border-primary !text-primary !w-32"
          onClick={onClose}
        >
          Batal
        </Button>
        <Button type="submit" className="!w-32">
          Konfirmasi
        </Button>
      </div>
    </form>
  );
}
