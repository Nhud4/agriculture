"use client";

import React, { useContext, useEffect, useState } from "react";
import Button from "@/components/modules/button";
import { ModalContext } from "@/components/context/ModalContext";
import InputText from "@/components/modules/inputText";
import Dropdown from "@/components/modules/dropdown";
import { useQuery, useMutation } from "@tanstack/react-query";
import { fetchListPlants } from "@/api/plants/plants.api";
import { fetchListRegion } from "@/api/region/region.api";
import {
  fetchEditPertanian,
  fetchDetailPertanian,
} from "@/api/report/report.api";

const defaultValues = {
  id: 0,
  tahun: "",
  kecId: 0,
  tanamanId: 0,
  tanam: 0,
  panen: 0,
  produksi: 0,
  provitas: "",
  clusterId: 0,
  createdBy: 0,
};

type Props = {
  token: string;
  id: number;
  onSuccess: () => void;
  onError: () => void;
};

export default function EditPertanian({
  token,
  id,
  onSuccess,
  onError,
}: Props) {
  const { onClose } = useContext(ModalContext);
  const [formValues, setFormValues] = useState(defaultValues);

  const { data } = useQuery({
    queryKey: ["pertanian/detail"],
    queryFn: async () => fetchDetailPertanian(token, id),
  });

  useEffect(() => {
    if (data) {
      setFormValues({ ...defaultValues, ...data });
    }
  }, [data]);

  const { data: plants } = useQuery({
    queryKey: ["plants/ops", [token]],
    queryFn: async () => fetchListPlants(token),
  });

  const { data: region } = useQuery({
    queryKey: ["region/ops", [token]],
    queryFn: async () => fetchListRegion(token),
  });

  const plantOps = plants?.map((item) => ({
    label: item.jenis,
    value: item.id,
  }));

  const regionOps = region?.map((item) => ({
    label: item.region,
    value: item.id,
  }));

  const { mutate } = useMutation({
    mutationKey: ["pertanian/edit"],
    mutationFn: (body: PertanianPayload) => {
      return fetchEditPertanian(token, id, body);
    },
    onSuccess: onSuccess,
    onError: onError,
  });

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    mutate(formValues);
  };

  return (
    <form onSubmit={onSubmit}>
      <div className="grid grid-cols-2 gap-4">
        <InputText
          label="Tahun"
          placeholder="Tahun"
          type="text"
          className="border border-neutral-3"
          value={formValues.tahun}
          onChange={(value) =>
            setFormValues({ ...formValues, tahun: `${value}` })
          }
        />
        <Dropdown
          label="Kecamatan"
          name="region"
          options={regionOps || []}
          placeholder="Kecamatan"
          onChange={(ops) =>
            setFormValues({ ...formValues, kecId: ops?.value as number })
          }
          value={regionOps?.filter((item) => item.value === formValues.kecId)}
          isDisabled
        />
        <Dropdown
          label="Jenis Tanaman"
          name="plants"
          options={plantOps || []}
          placeholder="Tanaman"
          onChange={(ops) =>
            setFormValues({ ...formValues, tanamanId: ops?.value as number })
          }
          value={plantOps?.filter(
            (item) => item.value === formValues.tanamanId
          )}
          isDisabled
        />
        <InputText
          label="Tanam"
          placeholder="Tanam"
          type="text"
          className="border border-neutral-3"
          value={`${formValues.tanam}`}
          onChange={(value) =>
            setFormValues({ ...formValues, tanam: value as number })
          }
          onlyNumber
          integer
        />
        <InputText
          label="Panen"
          placeholder="Panen"
          type="text"
          className="border border-neutral-3"
          value={`${formValues.panen}`}
          onChange={(value) =>
            setFormValues({ ...formValues, panen: value as number })
          }
          onlyNumber
          integer
        />
        <InputText
          label="Produksi"
          placeholder="Produksi"
          type="text"
          className="border border-neutral-3"
          value={`${formValues.produksi}`}
          onChange={(value) =>
            setFormValues({ ...formValues, produksi: value as number })
          }
          onlyNumber
          integer
        />
        <InputText
          label="Provitas"
          placeholder="Provitas"
          type="text"
          className="border border-neutral-3"
          value={formValues.provitas}
          onChange={(value) =>
            setFormValues({ ...formValues, provitas: value as string })
          }
          onlyNumber
        />
      </div>
      <div className="flex justify-end items-center space-x-4">
        <Button
          type="button"
          className="!bg-white border border-primary !text-primary !w-32"
          onClick={onClose}
        >
          Batal
        </Button>
        <Button type="submit" className="!w-32">
          Konfirmasi
        </Button>
      </div>
    </form>
  );
}
