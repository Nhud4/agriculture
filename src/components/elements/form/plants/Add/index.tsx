"use client";

import React, { useContext, useState } from "react";
import InputText from "@/components/modules/inputText";
import Button from "@/components/modules/button";
import { useMutation } from "@tanstack/react-query";
import { fetchAddPlants } from "@/api/plants/plants.api";
import Spinner from "@/components/elements/spinner";
import { ModalContext } from "@/components/context/ModalContext";

type Props = {
  token: string;
  handleSuccess: () => void;
  handleError: () => void;
};

const defaultValues = {
  jenis: "",
};

export default function FormAddPlants({
  token,
  handleSuccess,
  handleError,
}: Props) {
  const { onClose } = useContext(ModalContext);
  const [formValue, setFormValue] = useState(defaultValues);
  const [error, setError] = useState("");

  const { mutate, isPending } = useMutation({
    mutationKey: ["plants/add"],
    mutationFn: (body: PLansPayload) => {
      return fetchAddPlants(token, body);
    },
    onSuccess: handleSuccess,
    onError: handleError,
  });

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (!formValue.jenis) {
      setError("Jenis tanaman harus diisi");
    } else {
      mutate(formValue);
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <InputText
        label="Jenis Tanaman"
        placeholder="Jenis Tanaman"
        type="text"
        className="border border-neutral-3"
        value={formValue.jenis}
        onChange={(value) => setFormValue({ jenis: value as string })}
      />
      {error ? <p className="text-danger-500 pt-2">{error}</p> : null}
      <div className="flex justify-end items-center space-x-4 pt-4">
        <Button
          type="button"
          className="!bg-white border border-primary !text-primary !w-32"
          onClick={onClose}
        >
          Batal
        </Button>
        <Button type="submit" className="!w-32">
          {isPending ? <Spinner /> : "Konfirmasi"}
        </Button>
      </div>
    </form>
  );
}
