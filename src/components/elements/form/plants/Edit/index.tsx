"use client";

import React, { useContext, useState } from "react";
import { ModalContext } from "@/components/context/ModalContext";
import { useMutation } from "@tanstack/react-query";
import { fetchEditPlants } from "@/api/plants/plants.api";
import InputText from "@/components/modules/inputText";
import Button from "@/components/modules/button";
import Spinner from "@/components/elements/spinner";

type Props = {
  token: string;
  defaultValues: PlantsDetail;
  handleSuccess: () => void;
  handleError: () => void;
};

export default function FormEditPlants({
  token,
  defaultValues,
  handleSuccess,
  handleError,
}: Props) {
  const { onClose } = useContext(ModalContext);
  const [formValue, setFormValue] = useState(defaultValues);
  const [error, setError] = useState("");

  const { mutate, isPending } = useMutation({
    mutationKey: ["plants/edit"],
    mutationFn: (body: PlantsDetail) => {
      return fetchEditPlants(token, body.id, { jenis: body.jenis });
    },
    onSuccess: handleSuccess,
    onError: handleError,
  });

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (!formValue.jenis) {
      setError("Jenis tanaman harus diisi");
    } else {
      mutate(formValue);
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <div className="grid grid-cols-2 gap-4">
        <InputText
          label="ID"
          placeholder="ID"
          type="text"
          className="border border-neutral-3"
          value={formValue.code}
          isDisabled
        />
        <InputText
          label="Status"
          placeholder="Status"
          type="text"
          className="border border-neutral-3"
          value={formValue.active ? "Aktif" : "Non-Aktif"}
          isDisabled
        />
        <div className="col-span-2">
          <InputText
            label="Kecamatan"
            placeholder="Kecamatan"
            type="text"
            className="border border-neutral-3"
            value={formValue.jenis}
            onChange={(value) =>
              setFormValue({ ...formValue, jenis: value as string })
            }
          />
        </div>
      </div>
      {error ? <p className="text-danger-500 pt-2">{error}</p> : null}
      <div className="flex justify-end items-center space-x-4 pt-4">
        <Button
          type="button"
          className="!bg-white border border-primary !text-primary !w-32"
          onClick={onClose}
        >
          Batal
        </Button>
        <Button type="submit" className="!w-32">
          {isPending ? <Spinner /> : "Konfirmasi"}
        </Button>
      </div>
    </form>
  );
}
