"use client";

import React, { useEffect, useState } from "react";
import InputText from "@/components/modules/inputText";
import Button from "@/components/modules/button";
import { useRouter } from "next/navigation";
import { useMutation } from "@tanstack/react-query";
import { fetchLogin } from "@/api/auth/auth.api";
import { toast } from "react-toastify";

export default function LoginForm() {
  const [formValues, setFormValues] = useState({ username: "", password: "" });
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const { mutate } = useMutation({
    mutationFn: (body: LoginPayload) => {
      return fetchLogin(body);
    },
    onSuccess: (data) => {
      localStorage.setItem("token", data.accessToken);
      router.replace("/dashboard");
    },
    onError: () => {
      toast.error("Username atau password salah", {
        theme: "colored",
      });
      setLoading(false);
    },
  });

  const onSubmit = (e: { preventDefault: () => void }) => {
    e.preventDefault();
    if (!formValues.username || !formValues.password) {
      setError("Username atau password tidak boleh kosong");
    } else {
      setLoading(true);
      mutate(formValues);
    }
  };

  useEffect(() => {
    setError("");
  }, [formValues]);

  const isDisabled = Boolean(error);

  return (
    <div className="w-[60%]">
      <div className="text-white space-y-1 pb-8">
        <h1 className="text-3xl font-semibold">Halaman Login</h1>
        <p className="text-xl">Selamat datang dihalaman login</p>
        <p className="text-xl">
          PENENTUAN CLUSTER HASIL PERTANIAN KABUPATEN BOJONEGORO
        </p>
      </div>
      <form className="space-y-2" onSubmit={onSubmit}>
        <InputText
          label="Username"
          placeholder="username"
          type="text"
          labelClassName="text-white"
          value={formValues.username}
          onChange={(value) =>
            setFormValues({ ...formValues, username: value as string })
          }
        />
        <InputText
          label="Password"
          placeholder="password"
          type="password"
          labelClassName="text-white"
          value={formValues.password}
          onChange={(value) =>
            setFormValues({ ...formValues, password: value as string })
          }
        />
        {error ? <p className="text-yellow pt-2">{error}</p> : null}
        <div className="pt-6">
          <Button
            type="submit"
            isDisabled={isDisabled}
            className={isDisabled ? "cursor-not-allowed" : "cursor-pointer"}
          >
            {loading ? "..." : "Masuk"}
          </Button>
        </div>
      </form>
    </div>
  );
}
