"use client";

import React, { useContext, useState } from "react";
import InputText from "@/components/modules/inputText";
import { ModalContext } from "@/components/context/ModalContext";
import Button from "@/components/modules/button";
import Spinner from "@/components/elements/spinner";
import { useMutation } from "@tanstack/react-query";
import { fetchEditRegion } from "@/api/region/region.api";

type Props = {
  token: string;
  defaultValues: RegionDetail;
  handleSuccess: () => void;
  handleError: () => void;
};

export default function FormEditRegion({
  token,
  defaultValues,
  handleSuccess,
  handleError,
}: Props) {
  const { onClose } = useContext(ModalContext);
  const [formValue, setFormValue] = useState(defaultValues);
  const [error, setError] = useState("");

  const { mutate, isPending } = useMutation({
    mutationKey: ["region/edit"],
    mutationFn: (body: RegionDetail) => {
      return fetchEditRegion(token, body.id, { region: body.region });
    },
    onSuccess: handleSuccess,
    onError: handleError,
  });

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (!formValue.region) {
      setError("Kecamatan harus diisi");
    } else {
      mutate(formValue);
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <div className="grid grid-cols-2 gap-4">
        <InputText
          label="ID"
          placeholder="ID"
          type="text"
          className="border border-neutral-3"
          value={formValue.code}
          isDisabled
        />
        <InputText
          label="Status"
          placeholder="Status"
          type="text"
          className="border border-neutral-3"
          value={formValue.active ? "Aktif" : "Non-Aktif"}
          isDisabled
        />
        <div className="col-span-2">
          <InputText
            label="Kecamatan"
            placeholder="Kecamatan"
            type="text"
            className="border border-neutral-3"
            value={formValue.region}
            onChange={(value) =>
              setFormValue({ ...formValue, region: value as string })
            }
          />
        </div>
      </div>
      {error ? <p className="text-danger-500 pt-2">{error}</p> : null}
      <div className="flex justify-end items-center space-x-4 pt-4">
        <Button
          type="button"
          className="!bg-white border border-primary !text-primary !w-32"
          onClick={onClose}
        >
          Batal
        </Button>
        <Button type="submit" className="!w-32">
          {isPending ? <Spinner /> : "Konfirmasi"}
        </Button>
      </div>
    </form>
  );
}
