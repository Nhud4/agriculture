"use client";

import React, { useContext, useEffect, useMemo, useState } from "react";
import Dropdown from "@/components/modules/dropdown";
import Button from "@/components/modules/button";
import { ModalContext } from "@/components/context/ModalContext";
import { useMutation, useQuery } from "@tanstack/react-query";
import { fetchCountCluster } from "@/api/cluster/cluster.api";
import Spinner from "@/components/elements/spinner";
import { fetchListPlants } from "@/api/plants/plants.api";
import { fetchListRegion } from "@/api/region/region.api";

type Props = {
  token: string;
  handleSuccess: () => void;
  handleError: () => void;
};

const defaultValues = {
  tanamanId: 0,
  iterasi1: {
    c1: 0,
    c2: 0,
    c3: 0,
  },
  iterasi2: {
    c1: 0,
    c2: 0,
    c3: 0,
  },
};

export default function FormCountReport({
  token,
  handleSuccess,
  handleError,
}: Props) {
  const { onClose } = useContext(ModalContext);
  const [formValue, setFormValue] = useState(defaultValues);
  const [error, setError] = useState("");
  const isDisabled = Boolean(!formValue || error);

  const { data } = useQuery({
    queryKey: ["plants/list"],
    queryFn: async () => fetchListPlants(token),
  });

  const { data: region } = useQuery({
    queryKey: ["region/list"],
    queryFn: async () => fetchListRegion(token),
  });

  const { mutate, isPending } = useMutation({
    mutationKey: ["cluster/count"],
    mutationFn: (body: ClusterCountParams) => {
      return fetchCountCluster(body);
    },
    onSuccess: handleSuccess,
    onError: handleError,
  });

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (
      !formValue.tanamanId ||
      !formValue.iterasi1.c1 ||
      !formValue.iterasi1.c2 ||
      !formValue.iterasi1.c3 ||
      !formValue.iterasi2.c1 ||
      !formValue.iterasi2.c2 ||
      !formValue.iterasi2.c3
    ) {
      setError("Harap lengkapi data");
    } else {
      mutate(formValue);
    }
  };

  useEffect(() => {
    setError("");
  }, [formValue]);

  const dataOps = useMemo(() => {
    return data?.map((item) => ({
      label: item.jenis,
      value: item.id,
    }));
  }, [data]);

  const regionOps = useMemo(() => {
    return region?.map((item) => ({
      label: item.region,
      value: item.id,
    }));
  }, [region]);

  return (
    <form onSubmit={onSubmit}>
      <Dropdown
        name="tanaman"
        label="Tanaman"
        options={dataOps || []}
        placeholder="Pilih Tanaman"
        maxMenuHeight={85}
        onChange={(ops) =>
          setFormValue({ ...formValue, tanamanId: ops?.value as number })
        }
      />
      <div className="pt-4">
        <h1 className="font-semibold">Titik Iterasi 1</h1>
        <div className="grid grid-cols-2 gap-3 pt-2">
          <Dropdown
            name="TC1"
            label="Titik 1"
            options={regionOps || []}
            placeholder="Pilih Titik 1"
            maxMenuHeight={85}
            onChange={(ops) =>
              setFormValue({
                ...formValue,
                iterasi1: {
                  ...formValue.iterasi1,
                  c1: ops?.value as number,
                },
              })
            }
          />
          <Dropdown
            name="TC2"
            label="Titk 2"
            options={regionOps || []}
            placeholder="Pilih Titik 2"
            maxMenuHeight={85}
            onChange={(ops) =>
              setFormValue({
                ...formValue,
                iterasi1: {
                  ...formValue.iterasi1,
                  c2: ops?.value as number,
                },
              })
            }
          />
          <Dropdown
            name="TC3"
            label="Titik 3"
            options={regionOps || []}
            placeholder="Pilih Titik 3"
            maxMenuHeight={85}
            onChange={(ops) =>
              setFormValue({
                ...formValue,
                iterasi1: {
                  ...formValue.iterasi1,
                  c3: ops?.value as number,
                },
              })
            }
          />
        </div>
      </div>
      <div className="pt-4">
        <h1 className="font-semibold">Titik Iterasi 2</h1>
        <div className="grid grid-cols-2 gap-3 pt-2">
          <Dropdown
            name="TC4"
            label="Titik 4"
            options={regionOps || []}
            placeholder="Pilih Titik 4"
            maxMenuHeight={85}
            onChange={(ops) =>
              setFormValue({
                ...formValue,
                iterasi2: {
                  ...formValue.iterasi2,
                  c1: ops?.value as number,
                },
              })
            }
          />
          <Dropdown
            name="TC5"
            label="Titik 5"
            options={regionOps || []}
            placeholder="Pilih Titik 5"
            maxMenuHeight={85}
            onChange={(ops) =>
              setFormValue({
                ...formValue,
                iterasi2: {
                  ...formValue.iterasi2,
                  c2: ops?.value as number,
                },
              })
            }
          />
          <Dropdown
            name="TC6"
            label="Titik 6"
            options={regionOps || []}
            placeholder="Pilih Titik 6"
            maxMenuHeight={85}
            onChange={(ops) =>
              setFormValue({
                ...formValue,
                iterasi2: {
                  ...formValue.iterasi2,
                  c3: ops?.value as number,
                },
              })
            }
          />
        </div>
      </div>
      {error ? <p className="text-danger-500 pt-2">{error}</p> : null}
      <div className="flex justify-end items-center space-x-4 pt-4">
        <Button
          type="button"
          className="!bg-white border border-primary !text-primary !w-32"
          onClick={onClose}
        >
          Batal
        </Button>
        <Button
          type="submit"
          isDisabled={isDisabled}
          className={[
            "!w-32",
            isDisabled ? "cursor-not-allowed" : "cursor-pointer",
          ].join(" ")}
        >
          {isPending ? <Spinner /> : "Konfirmasi"}
        </Button>
      </div>
    </form>
  );
}
