"use client";

import React, { useEffect, useState } from "react";
import IMAGES from "@/config/images";
import Image from "next/image";
import ROUTES from "@/config/routes";
import { usePathname } from "next/navigation";
import { useQuery } from "@tanstack/react-query";
import { fetchProfile } from "@/api/auth/auth.api";

const NavBar: React.FC = () => {
  const [token, setToken] = useState("");
  const pathname = usePathname();
  const listUrl = ROUTES.filter((item) => item.path === pathname);

  const { data } = useQuery({
    queryKey: ["user/profile", token],
    queryFn: async () => fetchProfile(token),
  });

  useEffect(() => {
    setToken(localStorage.getItem("token") || "");
  }, []);

  return (
    <div className="fixed flex justify-between items-center pl-64 pr-4 py-2 w-full bg-white h-20 z-20 shadow">
      <div>
        <h1 className="text-xl font-bold">{listUrl[0].label}</h1>
        <p className="text-sm text-primary-2">{listUrl[0].label}</p>
      </div>
      <div className="flex justify-between items-center bg-white p-2 rounded-full shadow-md">
        <div className="px-4">
          <h1 className="text-sm font-semibold capitalize">{data?.nama}</h1>
          <p className="text-xs font-normal">Administrator</p>
        </div>
        <Image alt="profile" src={IMAGES.Profile} width={35} height={35} />
      </div>
    </div>
  );
};

export default NavBar;
