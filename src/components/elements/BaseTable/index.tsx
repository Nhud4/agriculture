"use client";

import React from "react";
import DataTable, { TableColumn } from "react-data-table-component";
import { tableStyles } from "./datatable";
import ICONS from "@/config/icons";
import styles from "./styles.module.css";
import IMAGES from "@/config/images";
import Image from "next/image";

type Props<T> = {
  columns: TableColumn<T>[];
  data: T[];
  title: string;
  actionComponent?: React.ReactElement;
  tableHight?: string;
  isLoading?: boolean;
  isSearch?: boolean;
};

const BaseTable: <T>(props: Props<T>) => React.ReactElement = ({
  columns,
  data,
  title,
  actionComponent,
  tableHight = "450px",
  isLoading,
  isSearch,
}) => {
  const hight =
    data?.length > 0 && tableHight !== "450px" ? tableHight : "450px";

  return (
    <div className="bg-white py-4 rounded-lg shadow">
      <div className="flex items-center justify-between pb-4 px-4">
        <h1 className="text-2xl font-medium">{title}</h1>
        <form className="flex items-center space-x-4">
          {isSearch ? (
            <div
              className={[
                styles.search,
                "flex items-center p-2 px-4 space-x-4 border border-transparent rounded-md border-[#E5E5E5]",
              ].join(" ")}
            >
              <ICONS.Search />
              <input
                className="text-sm focus:outline-none w-44"
                placeholder="Cari disini..."
                type="text"
              />
            </div>
          ) : null}

          {actionComponent && <div>{actionComponent}</div>}
        </form>
      </div>
      <div className="pl-2">
        <DataTable
          columns={columns}
          data={data}
          customStyles={tableStyles(false)}
          defaultSortAsc={false}
          fixedHeader
          fixedHeaderScrollHeight={hight}
          noDataComponent={
            <div className="grid w-full min-h-full bg-white place-items-center">
              <div className="flex flex-col items-center">
                <Image alt="empty" src={IMAGES.EmptyData} width={350} />
                <p className="text-2xl font-semibold text-center -mt-6">
                  Data tidak ditemukan.
                </p>
              </div>
            </div>
          }
          pagination
          progressPending={isLoading}
        />
      </div>
    </div>
  );
};

export default BaseTable;
