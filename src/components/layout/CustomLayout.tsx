"use client";

import React, { useEffect, useState } from "react";
import NavBar from "../elements/Navbar";
import Sidebar from "../elements/SideBar";
import { useRouter } from "next/navigation";

type Props = {
  children: React.ReactNode;
};

export default function CustomLayout({ children }: Props) {
  const router = useRouter();

  useEffect(() => {
    if (typeof window !== undefined) {
      const token = localStorage.getItem("token") || null;
      if (!token) {
        router.replace("/login");
      }
    }
  }, []);

  return (
    <div className="flex w-full overflow-hidden">
      <Sidebar />
      <div className="flex-1 px-0">
        <NavBar />
        <div className="ml-64 mt-24 mr-4">{children}</div>
      </div>
    </div>
  );
}
