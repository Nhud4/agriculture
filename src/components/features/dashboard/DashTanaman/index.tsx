"use client";

import React from "react";
import BarChart from "@/components/modules/barChart";

type Props = {
  data: GrafikTanaman[];
  loading: boolean;
};

export default function DashTanaman({ data, loading }: Props) {
  const labels = data.map((item) => item.tanaman);
  const tanaman = data.map((item) => item.tPanen);
  const panen = data.map((item) => item.tPanen);
  const produksi = data.map((item) => item.tProduksi);

  const chartData = {
    labels,
    datasets: [
      {
        label: "Tanam",
        data: tanaman,
        backgroundColor: "rgba(24, 20, 243, 1)",
        borderRadius: 5,
      },
      {
        label: "Panen",
        data: panen,
        backgroundColor: "rgb(255, 187, 56)",
        borderRadius: 5,
      },
      {
        label: "Produksi",
        data: produksi,
        backgroundColor: "rgb(22, 219, 204)",
        borderRadius: 5,
      },
    ],
  };

  return (
    <BarChart chartData={chartData} className="col-span-2" loading={loading} />
  );
}
