"use client";

import React from "react";
import LineChart from "@/components/modules/lineChart";

type Props = {
  data: GrafikKecamatan[];
  loading: boolean;
};

export default function DashRegion({ data, loading }: Props) {
  const labels = data.map((item) => item.kecamatan);
  const tanaman = data.map((item) => item.tPanen);
  const panen = data.map((item) => item.tPanen);
  const produksi = data.map((item) => item.tProduksi);

  const values = {
    labels,
    datasets: [
      {
        label: "Tanam",
        data: tanaman,
        borderColor: "rgb(76, 120, 255)",
        backgroundColor: "rgb(76, 120, 255)",
      },
      {
        label: "Panen",
        data: panen,
        borderColor: "rgb(255, 187, 56)",
        backgroundColor: "rgb(255, 187, 56)",
      },
      {
        label: "Produksi",
        data: produksi,
        borderColor: "rgb(22, 219, 204)",
        backgroundColor: "rgb(22, 219, 204)",
      },
    ],
  };

  return <LineChart data={values} loading={loading} />;
}
