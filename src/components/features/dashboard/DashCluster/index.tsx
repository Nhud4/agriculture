"use client";

import React from "react";
import PieChart from "@/components/modules/pieChart";

type Props = {
  data: GrafikCluster[];
  loading: boolean;
};

export default function DashCluster({ data, loading }: Props) {
  const labels = data.map((item) => item.cluster);
  const values = data.map((item) => item.count);

  const pirChartData = {
    labels,
    datasets: [
      {
        data: values,
        backgroundColor: [
          "rgba(52, 60, 106, 1)",
          "rgba(252, 121, 0, 1)",
          "rgba(24, 20, 243, 1)",
        ],
        borderWidth: 4,
      },
    ],
  };

  return (
    <PieChart
      chartData={pirChartData}
      label={labels as string & string[]}
      loading={loading}
    />
  );
}
