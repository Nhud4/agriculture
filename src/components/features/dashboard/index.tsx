"use client";

import React from "react";
import SummaryCard from "@/components/modules/summaryCard";
import PieChart from "@/components/modules/pieChart";
import LineChart from "@/components/modules/lineChart";
import { chartData, pirChartData } from "@/utils/dummies";
import { useQuery } from "@tanstack/react-query";
import {
  fetchSummary,
  fetchGrafikCluster,
  fetchGrafikKecamatan,
  fetchGrafikTanaman,
} from "@/api/dashboard/dashboard.api";
import DashTanaman from "./DashTanaman";
import DashCluster from "./DashCluster";
import DashRegion from "./DashRegion";

export default function DashboardComponent({ token }: TokenComponent) {
  const { data: summary } = useQuery({
    queryKey: ["dash/summary"],
    queryFn: async () => fetchSummary(token),
  });

  const { data: bar, isPending: loadBar } = useQuery({
    queryKey: ["dash/bar"],
    queryFn: async () => fetchGrafikTanaman(token),
  });

  const { data: cluster, isPending: loadCluster } = useQuery({
    queryKey: ["dash/cluster"],
    queryFn: async () => fetchGrafikCluster(token),
  });

  const { data: region, isPending: loadRegion } = useQuery({
    queryKey: ["dash/region"],
    queryFn: async () => fetchGrafikKecamatan(token),
  });

  return (
    <div className="space-y-4">
      <div className="grid grid-cols-4 gap-4">
        <SummaryCard
          color="blue"
          label="Data Pertanian"
          data={summary?.pertanian || 0}
        />
        <SummaryCard
          color="yellow"
          label="Data Kecamatan"
          data={summary?.kecamatan || 0}
        />
        <SummaryCard
          color="pink"
          label="Data Tanaman"
          data={summary?.tanaman || 0}
        />
        <SummaryCard
          color="cyn"
          label="Data Pelaporan"
          data={summary?.laporan || 0}
        />
      </div>
      <div className="grid grid-cols-3 gap-4">
        <DashTanaman data={bar || []} loading={loadBar} />
        <DashCluster data={cluster || []} loading={loadCluster} />
      </div>
      <DashRegion data={region || []} loading={loadRegion} />
    </div>
  );
}
