"use client";

import React, { useState } from "react";
import Dropdown from "@/components/modules/dropdown";
import Button from "@/components/modules/button";
import ICONS from "@/config/icons";
import { useQuery } from "@tanstack/react-query";
import { fetchListPlants } from "@/api/plants/plants.api";

type Props = {
  token: string;
  onFilter: (value?: string | number | boolean) => void;
  onAdd: () => void;
};

export const ActionsPertanian: React.FC<Props> = ({
  onFilter,
  onAdd,
  token,
}) => {
  const { data } = useQuery({
    queryKey: ["plants/ops", [token]],
    queryFn: async () => fetchListPlants(token),
  });

  const options = data?.map((item) => ({
    label: item.jenis,
    value: item.id,
  }));

  return (
    <div className="flex items-center space-x-2">
      <Dropdown
        name="plants"
        options={options || []}
        placeholder="Tanaman..."
        onChange={(ops) => onFilter(ops?.value)}
      />
      <Button className="flex items-center space-x-2" onClick={onAdd}>
        <ICONS.Plus />
        <p>Tambah Data</p>
      </Button>
    </div>
  );
};
