"use client";

import React, { useContext, useState } from "react";
import BaseTable from "@/components/elements/BaseTable";
import { columns } from "./Columns";
import { useQuery, useMutation } from "@tanstack/react-query";
import {
  fetchListPertanian,
  fetchDeletePertanian,
} from "@/api/report/report.api";
import { ActionsPertanian } from "./Actions";
import { ModalContext } from "@/components/context/ModalContext";
import { toast } from "react-toastify";
import { customMessage } from "@/utils/dummies";
import AddPertanian from "@/components/elements/form/pertanian/Add";
import EditPertanian from "@/components/elements/form/pertanian/Edit";
import Questions from "@/components/modules/question";

export default function TablePertanian({ token }: TokenComponent) {
  const { setModal, onClose } = useContext(ModalContext);
  const [plants, setPlants] = useState("");
  const [trigger, setTrigger] = useState("");

  const { data, isLoading } = useQuery({
    queryKey: ["laporan/list", [token, trigger, plants]],
    queryFn: async () =>
      fetchListPertanian(token, { tanamanId: plants as unknown as string }),
  });

  const onSuccess = (key: "add" | "edit" | "remove") => {
    toast.success(customMessage.success[key], {
      theme: "colored",
    });
    onClose();
    setTrigger(trigger + "0");
  };

  const onError = (key: "add" | "edit" | "remove") => {
    toast.error(customMessage.error[key], {
      theme: "colored",
    });
  };

  const { mutate } = useMutation({
    mutationKey: ["pertanian/remove"],
    mutationFn: (code: number) => {
      return fetchDeletePertanian(token, code) as Promise<"remove">;
    },
    onSuccess: () => onSuccess("remove"),
    onError: () => onError("remove"),
  });

  const handleAdd = () => {
    setModal({
      open: true,
      content: (
        <AddPertanian
          token={token}
          onSuccess={() => onSuccess("add")}
          onError={() => onError("add")}
        />
      ),
      title: "Tambah Data Petanian",
    });
  };

  const handleEdit = (id: number) => {
    setModal({
      open: true,
      content: (
        <EditPertanian
          id={id}
          token={token}
          onSuccess={() => onSuccess("edit")}
          onError={() => onError("edit")}
        />
      ),
      title: "Edit Data Petanian",
    });
  };

  const handleDelete = (id: number) => {
    setModal({
      open: true,
      content: (
        <Questions
          onClose={onClose}
          onConfirm={() => {
            mutate(id);
          }}
        />
      ),
      title: "Hapsu Data Petanian",
    });
  };

  const loading = isLoading;

  return (
    <BaseTable
      actionComponent={
        <ActionsPertanian
          token={token}
          onFilter={(value) => setPlants(value as string)}
          onAdd={handleAdd}
        />
      }
      columns={columns({
        loading,
        onEdit: (id) => handleEdit(id),
        onDelete: (id) => handleDelete(id),
      })}
      data={data as PertanianList[]}
      title="Data Pertanian"
      isLoading={loading}
    />
  );
}
