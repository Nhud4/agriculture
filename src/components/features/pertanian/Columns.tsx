"use client";

import React from "react";
import { TableCell } from "@/components/elements/BaseTable/TableCell";
import { TableColumn } from "react-data-table-component";
import PopUp from "@/components/modules/popUp";

type Props = {
  loading: boolean;
  onEdit: (id: number) => void;
  onDelete: (id: number) => void;
};

export const columns = ({
  loading,
  onEdit,
  onDelete,
}: Props): TableColumn<PertanianList>[] => [
  {
    name: "No",
    cell: ({ no }) => (
      <TableCell loading={loading} skeletonWidth={35} value={no?.toString()} />
    ),
    width: "70px",
  },
  {
    name: "Tahun",
    cell: ({ tahun }) => (
      <TableCell loading={loading} skeletonWidth={35} value={tahun} />
    ),
  },
  {
    name: "Kecamatan",
    cell: ({ kecamatan }) => (
      <TableCell loading={loading} skeletonWidth={35} value={kecamatan} />
    ),
  },
  {
    name: "Tanaman",
    cell: ({ jenis }) => (
      <TableCell loading={loading} skeletonWidth={35} value={jenis} />
    ),
  },
  {
    name: "Tanam",
    cell: ({ tanam }) => (
      <TableCell
        loading={loading}
        skeletonWidth={35}
        value={tanam?.toString()}
      />
    ),
  },
  {
    name: "Panen",
    cell: ({ panen }) => (
      <TableCell
        loading={loading}
        skeletonWidth={35}
        value={panen?.toString()}
      />
    ),
  },
  {
    name: "Produksi",
    cell: ({ produksi }) => (
      <TableCell
        loading={loading}
        skeletonWidth={35}
        value={produksi?.toString()}
      />
    ),
  },
  {
    name: "Provitas",
    cell: ({ provitas, id }) => (
      <TableCell
        loading={loading}
        skeletonWidth={35}
        value={
          <div className="flex justify-between items-center w-full">
            <p>{provitas}</p>
            <PopUp
              actions={["edit", "delete"]}
              onEdit={() => onEdit(id)}
              onDelete={() => onDelete(id)}
            />
          </div>
        }
      />
    ),
  },
];
