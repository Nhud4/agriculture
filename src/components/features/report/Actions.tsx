"use client";

import React from "react";
import Dropdown from "@/components/modules/dropdown";
import Button from "@/components/modules/button";
import { clusterOptions } from "@/utils/dummies";
import ICONS from "@/config/icons";
import { useQuery } from "@tanstack/react-query";
import { fetchListPlants } from "@/api/plants/plants.api";
import { fetchListCluster } from "@/api/cluster/cluster.api";

type Props = {
  token: string;
  onFilter: (value: PertanianParams) => void;
  onAdd: () => void;
  filterValue: PertanianParams;
};

export default function ReportActions({
  onFilter,
  onAdd,
  token,
  filterValue,
}: Props) {
  const { data } = useQuery({
    queryKey: ["plants/ops", [token]],
    queryFn: async () => fetchListPlants(token),
  });

  const { data: cluster } = useQuery({
    queryKey: ["cluster/list", [token]],
    queryFn: async () => fetchListCluster(token),
  });

  const options = data?.map((item) => ({
    label: item.jenis,
    value: item.id,
  }));

  const clusterOps =
    cluster?.map((item) => ({
      label: item.cluster,
      value: item.id,
    })) || [];

  return (
    <div className="flex items-center space-x-4">
      <Dropdown
        name="plants"
        options={options || []}
        placeholder="Tanaman..."
        onChange={(ops) =>
          onFilter({
            ...filterValue,
            tanamanId: ops === null ? "" : (ops.value as string),
          })
        }
      />
      <Dropdown
        name="plants"
        options={clusterOps}
        placeholder="Status..."
        onChange={(ops) =>
          onFilter({
            ...filterValue,
            clusterId: ops === null ? "" : (ops.value as string),
          })
        }
      />
      <Button
        type="button"
        className="flex justify-center items-center space-x-4 !bg-yellow"
        onClick={onAdd}
      >
        <ICONS.Refresh />
        <p>Hitung Data</p>
      </Button>
    </div>
  );
}
