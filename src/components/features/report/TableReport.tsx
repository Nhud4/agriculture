"use client";

import React, { useContext, useState } from "react";
import BaseTable from "@/components/elements/BaseTable";
import { columns } from "./Columns";
import { useQuery } from "@tanstack/react-query";
import { fetchListPertanian } from "@/api/report/report.api";
import ReportActions from "./Actions";
import { ModalContext } from "@/components/context/ModalContext";
import FormCountReport from "@/components/elements/form/report/count";
import { toast } from "react-toastify";
import { customMessage } from "@/utils/dummies";

const initialParams: PertanianParams = {
  clusterId: "",
  tanamanId: "",
};

export default function TableReport({ token }: TokenComponent) {
  const { setModal, onClose } = useContext(ModalContext);
  const [params, setParams] = useState(initialParams);
  const [trigger, setTrigger] = useState("");

  const { data, isLoading } = useQuery({
    queryKey: ["laporan/list", [params, token, trigger]],
    queryFn: async () => fetchListPertanian(token, params),
  });

  const onSuccess = (key: "add" | "edit" | "remove") => {
    toast.success(customMessage.success[key], {
      theme: "colored",
    });
    onClose();
    setTrigger(trigger + "0");
  };

  const onError = (key: "add" | "edit" | "remove") => {
    toast.error(customMessage.error[key], {
      theme: "colored",
    });
  };

  const handleAdd = () => {
    setModal({
      open: true,
      content: (
        <FormCountReport
          token={token}
          handleSuccess={() => onSuccess("add")}
          handleError={() => onError("add")}
        />
      ),
      title: "Hitung Algoritma",
    });
  };
  const loading = isLoading;

  return (
    <BaseTable
      actionComponent={
        <ReportActions
          onFilter={(value) => setParams({ ...params, ...value })}
          onAdd={handleAdd}
          token={token}
          filterValue={params}
        />
      }
      columns={columns(loading)}
      data={data as PertanianList[]}
      title="Data Hasil Perhitungan"
      isLoading={loading}
    />
  );
}
