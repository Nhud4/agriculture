"use client";

import React from "react";
import { TableCell } from "@/components/elements/BaseTable/TableCell";
import { TableColumn } from "react-data-table-component";
import Badge from "@/components/modules/badge";

export const columns = (loading: boolean): TableColumn<PertanianList>[] => [
  {
    name: "No",
    cell: ({ no }) => (
      <TableCell loading={loading} skeletonWidth={35} value={no?.toString()} />
    ),
    width: "70px",
  },
  {
    name: "Tahun",
    cell: ({ tahun }) => (
      <TableCell loading={loading} skeletonWidth={35} value={tahun} />
    ),
  },
  {
    name: "Kecamatan",
    cell: ({ kecamatan }) => (
      <TableCell loading={loading} skeletonWidth={35} value={kecamatan} />
    ),
  },
  {
    name: "Tanaman",
    cell: ({ jenis }) => (
      <TableCell loading={loading} skeletonWidth={35} value={jenis} />
    ),
  },
  {
    name: "Cluster",
    cell: ({ cluster, code }) => (
      <TableCell
        loading={loading}
        skeletonWidth={35}
        value={
          <div>
            {code === "C1" ? (
              <Badge className="bg-green-2 text-green">{cluster}</Badge>
            ) : null}
            {code === "C2" ? (
              <Badge className="bg-primary-4 text-primary">{cluster}</Badge>
            ) : null}
            {code === "C3" ? (
              <Badge className="bg-danger-50 text-danger-500">{cluster}</Badge>
            ) : null}
          </div>
        }
      />
    ),
  },
];
