"use client";

import React, { useContext, useState } from "react";
import BaseTable from "@/components/elements/BaseTable";
import { columns } from "./Columns";
import { useQuery, useMutation } from "@tanstack/react-query";
import { fetchListRegion, fetchDeleteRegion } from "@/api/region/region.api";
import { ModalContext } from "@/components/context/ModalContext";
import Button from "@/components/modules/button";
import ICONS from "@/config/icons";
import { toast } from "react-toastify";
import { customMessage } from "@/utils/dummies";
import FormAddRegion from "@/components/elements/form/region/Add";
import FormEditRegion from "@/components/elements/form/region/Edit";
import Questions from "@/components/modules/question";

const TableKecamatan: React.FC<TokenComponent> = ({ token }) => {
  const { setModal, onClose } = useContext(ModalContext);
  const [trigger, setTrigger] = useState("");

  const { data, isLoading } = useQuery({
    queryKey: ["region/list", [token, trigger]],
    queryFn: async () => fetchListRegion(token),
  });

  const loading = isLoading;

  const onSuccess = (key: "add" | "edit" | "remove") => {
    toast.success(customMessage.success[key], {
      theme: "colored",
    });
    onClose();
    setTrigger(trigger + "0");
  };

  const onError = (key: "add" | "edit" | "remove") => {
    toast.error(customMessage.error[key], {
      theme: "colored",
    });
  };

  const { mutate } = useMutation({
    mutationKey: ["region/remove"],
    mutationFn: (code: number) => {
      return fetchDeleteRegion(token, code) as Promise<"remove">;
    },
    onSuccess: () => onSuccess("remove"),
    onError: () => onError("remove"),
  });

  const handleAdd = () => {
    setModal({
      open: true,
      content: (
        <FormAddRegion
          token={token}
          handleSuccess={() => onSuccess("add")}
          handleError={() => onError("add")}
        />
      ),
      title: "Tambah Data Kecamatan",
    });
  };

  const handleEdit = (value: RegionDetail) => {
    setModal({
      open: true,
      content: (
        <FormEditRegion
          token={token}
          handleSuccess={() => onSuccess("edit")}
          handleError={() => onError("edit")}
          defaultValues={value}
        />
      ),
      title: "Edit Data Kecamatan",
    });
  };

  const handleDelete = (id: number) => {
    setModal({
      open: true,
      content: <Questions onClose={onClose} onConfirm={() => mutate(id)} />,
      title: "Hapus Data Kecamatan",
    });
  };

  return (
    <BaseTable
      actionComponent={
        <Button className="flex items-center space-x-2" onClick={handleAdd}>
          <ICONS.Plus />
          <p>Tambah Data</p>
        </Button>
      }
      columns={columns({
        loading,
        onEdit: (value) => handleEdit(value),
        onDelete: (id) => handleDelete(id),
      })}
      data={data as RegionLis[]}
      title="Data Kecamatan"
      tableHight="350px"
      isLoading={loading}
    />
  );
};

export default TableKecamatan;
