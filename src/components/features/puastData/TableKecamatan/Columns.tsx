"use client";

import React from "react";
import { TableCell } from "@/components/elements/BaseTable/TableCell";
import { TableColumn } from "react-data-table-component";
import Badge from "@/components/modules/badge";
import PopUp from "@/components/modules/popUp";

type Props = {
  loading: boolean;
  onEdit: (value: RegionDetail) => void;
  onDelete: (id: number) => void;
};

export const columns = ({
  loading,
  onEdit,
  onDelete,
}: Props): TableColumn<RegionLis>[] => [
  {
    name: "No",
    cell: ({ no }) => (
      <TableCell loading={loading} skeletonWidth={35} value={no?.toString()} />
    ),
    width: "70px",
  },
  {
    name: "ID",
    cell: ({ code }) => (
      <TableCell loading={loading} skeletonWidth={35} value={code} />
    ),
  },
  {
    name: "Kecamatan",
    cell: ({ region }) => (
      <TableCell loading={loading} skeletonWidth={35} value={region} />
    ),
  },
  {
    name: "Status",
    cell: ({ active, id, code, region }) => (
      <TableCell
        loading={loading}
        skeletonWidth={35}
        value={
          <div className="flex justify-between items-center w-full">
            <Badge
              className={
                active
                  ? "bg-primary-4 text-primary"
                  : "bg-danger-50 text-danger-500"
              }
            >
              {active ? "Aktif" : "Non-Aktif"}
            </Badge>
            <PopUp
              actions={active ? ["edit"] : ["edit", "delete"]}
              onEdit={() => onEdit({ id, code, region, active })}
              onDelete={() => onDelete(id)}
            />
          </div>
        }
      />
    ),
  },
];
