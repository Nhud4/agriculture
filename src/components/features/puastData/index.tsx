"use client";

import React, { useContext, useState } from "react";
import TableKecamatan from "./TableKecamatan/TableKecamatan";
import TableTanaman from "./TableTanaman/TableTanaman";
import TableCluster from "./TableCLuster/TableCluster";
import Button from "@/components/modules/button";
import ICONS from "@/config/icons";
import { ModalContext } from "@/components/context/ModalContext";

export default function PusatData({ token }: TokenComponent) {
  const [showData, setShowData] = useState("1");
  const { setModal } = useContext(ModalContext);

  const handleAdd = () => {
    setModal({
      open: true,
      content: <div>Hallo</div>,
      title: "Tambah nih",
    });
  };

  return (
    <>
      <div className="flex justify-between items-center bg-white mb-4 p-4 rounded-lg">
        <div className="space-y-1">
          <h1 className="text-lg font-semibold">Data Yang ditampilkan</h1>
          <div className="flex items-center space-x-8">
            <div className="flex items-center space-x-2">
              <input
                checked={showData === "1"}
                type="radio"
                className="w-fit"
                name="showData"
                value="1"
                onChange={(e) => setShowData(e.target.value)}
              />
              <h4 className="text-base">Kecamatan</h4>
            </div>
            <div className="flex items-center space-x-2">
              <input
                checked={showData === "2"}
                type="radio"
                className="w-fit"
                name="showData"
                value="2"
                onChange={(e) => setShowData(e.target.value)}
              />
              <h4 className="text-base">Tanaman</h4>
            </div>
            <div className="flex items-center space-x-2">
              <input
                checked={showData === "3"}
                type="radio"
                className="w-fit"
                name="showData"
                value="3"
                onChange={(e) => setShowData(e.target.value)}
              />
              <h4 className="text-base">Cluster</h4>
            </div>
          </div>
        </div>
      </div>
      {showData === "1" ? <TableKecamatan token={token} /> : null}
      {showData === "2" ? <TableTanaman token={token} /> : null}
      {showData === "3" ? <TableCluster token={token} /> : null}
    </>
  );
}
