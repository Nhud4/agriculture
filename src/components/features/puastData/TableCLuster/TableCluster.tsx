"use client";

import React, { useContext, useState } from "react";
import BaseTable from "@/components/elements/BaseTable";
import { columns } from "./Columns";
import { useQuery, useMutation } from "@tanstack/react-query";
import {
  fetchListCluster,
  fetchDeleteCluster,
} from "@/api/cluster/cluster.api";
import Button from "@/components/modules/button";
import ICONS from "@/config/icons";
import { toast } from "react-toastify";
import { customMessage } from "@/utils/dummies";
import Questions from "@/components/modules/question";
import { ModalContext } from "@/components/context/ModalContext";
import FormAddCluster from "@/components/elements/form/cluster/Add";
import FormEditCluster from "@/components/elements/form/cluster/Edit/inde";

export default function TableCluster({ token }: TokenComponent) {
  const { setModal, onClose } = useContext(ModalContext);
  const [trigger, setTrigger] = useState("");

  const { data, isLoading } = useQuery({
    queryKey: ["cluster/list/table", [token, trigger]],
    queryFn: async () => fetchListCluster(token),
  });

  const loading = isLoading;

  const onSuccess = (key: "add" | "edit" | "remove") => {
    toast.success(customMessage.success[key], {
      theme: "colored",
    });
    onClose();
    setTrigger(trigger + "0");
  };

  const onError = (key: "add" | "edit" | "remove") => {
    toast.error(customMessage.error[key], {
      theme: "colored",
    });
  };

  const { mutate } = useMutation({
    mutationKey: ["cluster/remove"],
    mutationFn: (code: number) => {
      return fetchDeleteCluster(token, code);
    },
    onSuccess: () => onSuccess("remove"),
    onError: () => onError("remove"),
  });

  const handleAdd = () => {
    setModal({
      open: true,
      content: (
        <FormAddCluster
          token={token}
          handleSuccess={() => onSuccess("add")}
          handleError={() => onError("add")}
        />
      ),
      title: "Tambah Data Cluster",
    });
  };

  const handleEdit = (value: ClusterDetail) => {
    setModal({
      open: true,
      content: (
        <FormEditCluster
          token={token}
          defaultValues={value}
          handleSuccess={() => onSuccess("edit")}
          handleError={() => onError("edit")}
        />
      ),
      title: "Edit Data Cluster",
    });
  };

  const handleDelete = (id: number) => {
    setModal({
      open: true,
      content: <Questions onClose={onClose} onConfirm={() => mutate(id)} />,
      title: "Hapus Data Cluster",
    });
  };

  return (
    <BaseTable
      actionComponent={
        <Button className="flex items-center space-x-2" onClick={handleAdd}>
          <ICONS.Plus />
          <p>Tambah Data</p>
        </Button>
      }
      columns={columns({
        loading,
        onEdit: (value) => handleEdit(value),
        onDelete: (id) => handleDelete(id),
      })}
      data={data as CLusterList[]}
      title="Cluster Pertanian"
      tableHight="350px"
      isLoading={loading}
    />
  );
}
