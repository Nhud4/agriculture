"use client";

import React from "react";
import { TableCell } from "@/components/elements/BaseTable/TableCell";
import { TableColumn } from "react-data-table-component";
import Badge from "@/components/modules/badge";
import PopUp from "@/components/modules/popUp";

type Props = {
  loading: boolean;
  onEdit: (value: PlantsDetail) => void;
  onDelete: (id: number) => void;
};

export const columns = ({
  loading,
  onEdit,
  onDelete,
}: Props): TableColumn<PlansList>[] => [
  {
    name: "No",
    cell: ({ no }) => (
      <TableCell loading={loading} skeletonWidth={35} value={no?.toString()} />
    ),
    width: "70px",
  },
  {
    name: "ID",
    cell: ({ code }) => (
      <TableCell loading={loading} skeletonWidth={35} value={code} />
    ),
  },
  {
    name: "Jenis Tanaman",
    cell: ({ jenis }) => (
      <TableCell loading={loading} skeletonWidth={35} value={jenis} />
    ),
  },
  {
    name: "Status",
    cell: ({ active, id, code, jenis }) => (
      <TableCell
        loading={loading}
        skeletonWidth={35}
        value={
          <div className="flex justify-between items-center w-full">
            <Badge
              className={
                active
                  ? "bg-primary-4 text-primary"
                  : "bg-danger-50 text-danger-500"
              }
            >
              {active ? "Aktif" : "Non-Aktif"}
            </Badge>
            <PopUp
              actions={active ? ["edit"] : ["edit", "delete"]}
              onEdit={() => onEdit({ id, code, jenis, active })}
              onDelete={() => onDelete(id)}
            />
          </div>
        }
      />
    ),
  },
];
