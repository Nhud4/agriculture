"use client";

import React, { useContext, useState } from "react";
import BaseTable from "@/components/elements/BaseTable";
import { columns } from "./Columns";
import { useQuery, useMutation } from "@tanstack/react-query";
import { fetchListPlants, fetchDeletePlants } from "@/api/plants/plants.api";
import { ModalContext } from "@/components/context/ModalContext";
import Button from "@/components/modules/button";
import ICONS from "@/config/icons";
import { toast } from "react-toastify";
import { customMessage } from "@/utils/dummies";
import Questions from "@/components/modules/question";
import FormAddPlants from "@/components/elements/form/plants/Add";
import FormEditPlants from "@/components/elements/form/plants/Edit";

const TableTanaman: React.FC<TokenComponent> = ({ token }) => {
  const { setModal, onClose } = useContext(ModalContext);
  const [trigger, setTrigger] = useState("");

  const { data, isLoading } = useQuery({
    queryKey: ["plants/list", [token, trigger]],
    queryFn: async () => fetchListPlants(token),
  });

  const onSuccess = (key: "add" | "edit" | "remove") => {
    toast.success(customMessage.success[key], {
      theme: "colored",
    });
    onClose();
    setTrigger(trigger + "0");
  };

  const { mutate } = useMutation({
    mutationKey: ["plants/remove"],
    mutationFn: (code: number) => {
      return fetchDeletePlants(token, code);
    },
    onSuccess: () => onSuccess("remove"),
    onError: () => onError("remove"),
  });

  const onError = (key: "add" | "edit" | "remove") => {
    toast.error(customMessage.error[key], {
      theme: "colored",
    });
  };

  const handleAdd = () => {
    setModal({
      open: true,
      content: (
        <FormAddPlants
          token={token}
          handleSuccess={() => onSuccess("add")}
          handleError={() => onError("add")}
        />
      ),
      title: "Tambah Data Tanaman",
    });
  };

  const handleEdit = (value: PlantsDetail) => {
    setModal({
      open: true,
      content: (
        <FormEditPlants
          token={token}
          defaultValues={value}
          handleSuccess={() => onSuccess("edit")}
          handleError={() => onError("edit")}
        />
      ),
      title: "Edit Data Tanaman",
    });
  };

  const handleDelete = (id: number) => {
    setModal({
      open: true,
      content: <Questions onClose={onClose} onConfirm={() => mutate(id)} />,
      title: "Hapus Data Tanaman",
    });
  };

  const loading = isLoading;
  return (
    <BaseTable
      actionComponent={
        <Button className="flex items-center space-x-2" onClick={handleAdd}>
          <ICONS.Plus />
          <p>Tambah Data</p>
        </Button>
      }
      columns={columns({
        loading,
        onEdit: (value) => handleEdit(value),
        onDelete: (id) => handleDelete(id),
      })}
      data={data as PlansList[]}
      title="Data Tanaman"
      tableHight="350px"
      isLoading={loading}
    />
  );
};

export default TableTanaman;
