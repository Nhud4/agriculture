"use client";

import React, { useState, useEffect } from "react";
import InputText from "@/components/modules/inputText";
import Button from "@/components/modules/button";
import { useQuery, useMutation } from "@tanstack/react-query";
import { fetchDetailUser, fetchUpdateUser } from "@/api/auth/auth.api";
import { toast } from "react-toastify";
import { customMessage } from "@/utils/dummies";
import Spinner from "@/components/elements/spinner";

const defaultValues: DetailUser = {
  id: 0,
  name: "",
  username: "",
  userCategory: "",
  password: "",
};

type Props = {
  token: string;
  id: number;
  trigger: string;
  handleSuccess: (key: "add" | "edit" | "remove") => void;
  handleError: (key: "add" | "edit" | "remove") => void;
};

const UserProfile: React.FC<Props> = ({
  id,
  token,
  trigger,
  handleSuccess,
  handleError,
}) => {
  const [formValues, setFormValues] = useState({
    ...defaultValues,
    newPassword: "",
  });
  const [error, setError] = useState("");
  const [isEdit, setIsEdit] = useState(false);

  const { data } = useQuery({
    queryKey: ["user/detail", [id, token, trigger]],
    queryFn: async () => fetchDetailUser(token, id),
  });

  useEffect(() => {
    if (data) {
      setIsEdit(false);
      setFormValues({ ...data, newPassword: data.password });
    }
  }, [data]);

  const { mutate, isPending } = useMutation({
    mutationKey: ["user/edit"],
    mutationFn: (body: UserPayload) => {
      return fetchUpdateUser(token, id, body);
    },
    onSuccess: () => handleSuccess("edit"),
    onError: () => handleError("edit"),
  });

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    if (
      !formValues.name ||
      !formValues.username ||
      !formValues.password ||
      !formValues.newPassword
    ) {
      setError("Harap melengkapi data");
    } else {
      mutate({
        username: formValues.username,
        name: formValues.name,
        oldPassword: formValues.password,
        newPassword: formValues.newPassword,
      });
    }
  };

  return (
    <div className="bg-white p-4 rounded-lg h-fit">
      <h1 className="text-lg font-semibold">Informasi Pengguna</h1>
      <form className="space-y-2" onSubmit={onSubmit}>
        <InputText
          type="text"
          label="Nama"
          placeholder="name"
          value={formValues.name}
          onChange={(value) =>
            setFormValues({ ...formValues, name: value as string })
          }
          className="border border-neutral"
          isDisabled={!isEdit}
        />
        <InputText
          type="text"
          label="Username"
          placeholder="username"
          value={formValues.username}
          onChange={(value) =>
            setFormValues({ ...formValues, username: value as string })
          }
          className="border border-neutral"
          isDisabled={!isEdit}
        />
        <InputText
          label="Password"
          placeholder="password"
          type="password"
          value={formValues.password}
          onChange={(value) =>
            setFormValues({ ...formValues, password: value as string })
          }
          className="border border-neutral"
          isDisabled={true}
        />
        <InputText
          label="Password Baru"
          placeholder="password Baru"
          type="password"
          value={formValues.newPassword}
          onChange={(value) =>
            setFormValues({ ...formValues, newPassword: value as string })
          }
          className="border border-neutral"
          isDisabled={!isEdit}
        />
        {error ? <p className="text-danger-500 pt-2">{error}</p> : null}
        <div className="flex justify-end w-full pt-6">
          {isEdit ? (
            <div className="space-x-2">
              <Button
                type="button"
                className="!w-fit !bg-white border !border-primary !text-primary"
                onClick={() => setIsEdit(false)}
              >
                Batal
              </Button>
              <Button type="submit" className="!w-fit">
                {isPending ? <Spinner /> : "Konfirmasi"}
              </Button>
            </div>
          ) : (
            <Button
              type="button"
              className="!w-fit"
              onClick={() => setIsEdit(true)}
            >
              Ubah
            </Button>
          )}
        </div>
      </form>
    </div>
  );
};

export default UserProfile;
