"use client";

import React, { useContext, useState } from "react";
import TableUser from "./TableUser";
import UserProfile from "./UserProfile";
import { useQuery } from "@tanstack/react-query";
import { fetchProfile } from "@/api/auth/auth.api";
import { toast } from "react-toastify";
import { customMessage } from "@/utils/dummies";

type Props = {
  token: string;
};

export default function UserData({ token }: Props) {
  const [trigger, setTrigger] = useState("");
  const [isEdit, setIsEdit] = useState(false);

  const { data } = useQuery({
    queryKey: ["user/profile", token],
    queryFn: async () => fetchProfile(token),
  });

  const handleSuccess = (key: "add" | "edit" | "remove") => {
    toast.success(customMessage.success[key], {
      theme: "colored",
    });
    setIsEdit(false);
    setTrigger(trigger + 0);
  };

  const handleError = (key: "add" | "edit" | "remove") => {
    toast.error(customMessage.error[key], {
      theme: "colored",
    });
  };

  return (
    <div className="grid grid-cols-3 gap-4">
      <UserProfile
        token={token}
        id={data?.id as number}
        trigger={trigger}
        handleSuccess={(key) => handleSuccess(key)}
        handleError={(key) => handleError(key)}
      />
      <div className="col-span-2">
        <TableUser
          token={token}
          trigger={trigger}
          handleSuccess={(key) => handleSuccess(key)}
          handleError={(key) => handleError(key)}
        />
      </div>
    </div>
  );
}
