"use client";

import React from "react";
import { TableCell } from "@/components/elements/BaseTable/TableCell";
import { TableColumn } from "react-data-table-component";
import PopUp from "@/components/modules/popUp";

type Props = {
  loading: boolean;
  onDetail: (code: number) => void;
  onDelete: (code: number) => void;
};

export const columns = ({
  loading,
  onDetail,
  onDelete,
}: Props): TableColumn<ListUser>[] => [
  {
    name: "No",
    cell: ({ id }) => (
      <TableCell loading={loading} skeletonWidth={35} value={id?.toString()} />
    ),
    width: "70px",
  },
  {
    name: "Nama",
    cell: ({ nama }) => (
      <TableCell loading={loading} skeletonWidth={35} value={nama} />
    ),
  },
  {
    name: "Username",
    cell: ({ username, id }) => (
      <TableCell
        loading={loading}
        skeletonWidth={35}
        value={
          <div className="flex justify-between items-center w-full">
            <p>{username}</p>
            <PopUp
              actions={["detail", "delete"]}
              onDetail={() => onDetail(id)}
              onDelete={() => onDelete(id)}
            />
          </div>
        }
      />
    ),
  },
];
