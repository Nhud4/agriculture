"use client";

import React, { useContext, useState } from "react";
import BaseTable from "@/components/elements/BaseTable";
import { columns } from "./Columns";
import Button from "@/components/modules/button";
import ICONS from "@/config/icons";
import { ModalContext } from "@/components/context/ModalContext";
import AddUserForm from "@/components/elements/form/user/Add";
import { useQuery, useMutation } from "@tanstack/react-query";
import { fetchListUser, fetchRemoveUser } from "@/api/auth/auth.api";
import DetailUser from "@/components/elements/form/user/Detail";
import Questions from "@/components/modules/question";

type Props = {
  token: string;
  trigger: string;
  handleSuccess: (key: "add" | "edit" | "remove") => void;
  handleError: (key: "add" | "edit" | "remove") => void;
};

const TableUser: React.FC<Props> = ({
  token,
  trigger,
  handleSuccess,
  handleError,
}) => {
  const { setModal, onClose } = useContext(ModalContext);

  const { isLoading, data } = useQuery({
    queryKey: ["user/list", [token, trigger]],
    queryFn: async () => fetchListUser(token),
  });

  const { mutate } = useMutation({
    mutationKey: ["user/remove"],
    mutationFn: (code: number) => {
      return fetchRemoveUser(token, code) as Promise<"remove">;
    },
    onSuccess: () => handleSuccess("remove"),
    onError: () => handleError("remove"),
  });

  const handleAdd = () => {
    setModal({
      open: true,
      content: (
        <AddUserForm
          handleSuccess={() => handleSuccess("add")}
          handleError={() => handleError("add")}
        />
      ),
      title: "Tmbah Pengguna",
    });
  };

  const handleDetail = (code: number) => {
    setModal({
      open: true,
      content: <DetailUser token={token} id={code} />,
      title: "Detail Pengguna",
    });
  };

  const handleDelete = (code: number) => {
    setModal({
      open: true,
      content: (
        <Questions
          onClose={onClose}
          onConfirm={() => {
            mutate(code);
          }}
        />
      ),
      title: "Hapus Pengguna",
    });
  };

  const loading = isLoading;

  return (
    <div>
      <BaseTable
        actionComponent={
          <Button
            type="button"
            className="flex items-center space-x-2"
            onClick={handleAdd}
          >
            <ICONS.Plus />
            <p>Tambah Data</p>
          </Button>
        }
        columns={columns({
          loading,
          onDetail: (code) => handleDetail(code),
          onDelete: (code) => handleDelete(code),
        })}
        data={data as ListUser[]}
        title="Data Pengguna"
        isLoading={loading}
      />
    </div>
  );
};

export default TableUser;
