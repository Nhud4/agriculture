"use client";

import React from "react";
import ICONS from "@/config/icons";

type Props = {
  onClose: () => void;
  open: boolean;
  title?: string;
  children: React.ReactNode;
};

const Modal: React.FC<Props> = ({ children, title, onClose, open }) => {
  if (open) {
    return (
      <div className="absolute inset-0 z-50 flex items-center justify-center bg-opacity-25 bg-gray-3">
        <div className="min-w-[600px] py-4 bg-white rounded-md">
          <div className="flex flex-row-reverse items-center justify-between px-4 pb-4 border-b border-neutral">
            <button onClick={onClose}>
              <ICONS.Cross />
            </button>
            {title ? (
              <h5 className="mt-1 ml-2 font-semibold">{title}</h5>
            ) : null}
          </div>
          <div className="px-4 mt-4">{children}</div>
        </div>
      </div>
    );
  }

  return null;
};

export default Modal;
