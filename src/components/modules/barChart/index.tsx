"use client";

import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  ChartOptions,
  ChartData,
} from "chart.js";
import { Bar } from "react-chartjs-2";

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip);

export const options = {
  responsive: true,
  onHover: (event, element) => {
    const canvas: any = event.native?.target;
    canvas.style.cursor = element[0] ? "pointer" : "default";
  },
  plugins: {
    legend: {
      display: false,
    },
  },
  layout: {
    padding: {
      top: 40,
    },
  },
  scales: {
    y: {
      grid: {
        borderDash: [15],
      },
    },
    x: {
      grid: {
        display: false,
      },
    },
  },
} as ChartOptions<"bar">;

type Props = {
  chartData: ChartData<"bar", number[], string>;
  className?: string;
  loading: boolean;
};

const BarChart: React.FC<Props> = ({ chartData, className, loading }) => {
  return (
    <div className={["bg-white p-4 rounded-lg", className].join(" ")}>
      <h1 className="text-lg font-medium text-primary-2">
        Grafik Jumlah Tanaman
      </h1>
      {loading ? (
        <div className="flex justify-center items-center h-full">
          <h1 className="text-2xl font-bold">Loading...</h1>
        </div>
      ) : (
        <Bar options={options} data={chartData} />
      )}
    </div>
  );
};

export default BarChart;
