"use client";

import React, { useState } from "react";
import ICONS from "@/config/icons";

type Props = {
  actions?: Actions[];
  onDetail?: () => void;
  onEdit?: () => void;
  onDelete?: () => void;
};

export default function PopUp({ actions, onDetail, onEdit, onDelete }: Props) {
  const [isOpen, setIsOpen] = useState(false);

  const open = Boolean(isOpen && actions && actions?.length > 0);

  return (
    <div>
      <button
        className="bg-white rounded-full p-1 shadow"
        onClick={() => setIsOpen(!isOpen)}
      >
        <ICONS.Dote />
      </button>
      <button onClick={() => setIsOpen(false)}>
        {open ? (
          <div className="absolute z-30 bg-white rounded-lg shadow-lg right-8 w-36 border border-neutral-2">
            {actions?.includes("detail") ? (
              <button
                className="flex items-center space-x-2 w-full py-2 px-3 border-b border-neutral"
                onClick={onDetail}
              >
                <ICONS.Eyes />
                <p className="text-sm">Lihat</p>
              </button>
            ) : null}
            {actions?.includes("edit") ? (
              <button
                className="flex items-center space-x-2 w-full py-2 px-3 border-b border-neutral"
                onClick={onEdit}
              >
                <ICONS.Edit />
                <p className="text-sm">Edit</p>
              </button>
            ) : null}
            {actions?.includes("delete") ? (
              <button
                className="flex items-center space-x-2 w-full py-2 px-3"
                onClick={onDelete}
              >
                <ICONS.Close />
                <p className="text-danger-500 text-sm">Hapus</p>
              </button>
            ) : null}
          </div>
        ) : null}
      </button>
    </div>
  );
}
