"use client";

import React from "react";

type Props = {
  children: React.ReactNode;
  type?: "button" | "submit" | "reset";
  onClick?: () => void;
  isDisabled?: boolean;
  className?: string;
};

const Button: React.FC<Props> = ({
  children,
  type,
  onClick,
  isDisabled,
  className,
}): React.ReactElement => {
  return (
    <button
      disabled={isDisabled}
      type={type ? type : "button"}
      onClick={onClick}
      className={[
        "bg-primary-2 text-white w-full p-2 rounded-lg",
        isDisabled ? "opacity-75" : "",
        className,
      ].join(" ")}
    >
      {children}
    </button>
  );
};

export default Button;
