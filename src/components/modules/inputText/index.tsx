"use client";

import React, { useState } from "react";
import ICONS from "@/config/icons";

type Props = {
  label: string;
  placeholder: string;
  type: "text" | "password";
  labelClassName?: string;
  onChange?: (value: string | number) => void;
  value?: string;
  className?: string;
  isDisabled?: boolean;
  onlyNumber?: boolean;
  integer?: boolean;
};

const InputText: React.FC<Props> = ({
  label,
  placeholder,
  type,
  labelClassName,
  onChange,
  value,
  className,
  isDisabled,
  onlyNumber,
  integer,
}): React.ReactElement => {
  const isPassword = type === "password";
  const [showPass, setShowPass] = useState(false);

  const transform = (value: string) => {
    if (onlyNumber) {
      return integer
        ? Number(value.replace(/[^0-9]/g, ""))
        : value.replace(/[^\d.]/g, "");
    }

    return value;
  };

  return (
    <div>
      <h1 className={["text-base font-medium pb-2", labelClassName].join(" ")}>
        {label}
      </h1>
      {isPassword ? (
        <div
          className={[
            "flex items-center space-x-2 py-2 px-3 rounded-lg",
            className,
            isDisabled ? "bg-neutral-2 opacity-50" : "bg-white",
          ].join(" ")}
        >
          <input
            onChange={onChange ? (e) => onChange(e.target.value) : () => {}}
            value={value}
            type={showPass ? "text" : "password"}
            placeholder={placeholder}
            className="w-full"
            disabled={isDisabled}
          />
          <button
            onClick={() => setShowPass(!showPass)}
            type="button"
            disabled={isDisabled}
          >
            <ICONS.Eyes />
          </button>
        </div>
      ) : (
        <div
          className={[
            "flex items-center space-x-2 py-2 px-3 rounded-lg",
            className,
            isDisabled ? "bg-neutral-2 opacity-50" : "bg-white",
          ].join(" ")}
        >
          <input
            onChange={
              onChange ? (e) => onChange(transform(e.target.value)) : () => {}
            }
            value={value}
            type="text"
            placeholder={placeholder}
            disabled={isDisabled}
          />
        </div>
      )}
    </div>
  );
};

export default InputText;
