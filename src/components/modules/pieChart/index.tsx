"use client";

import React, { HTMLProps } from "react";
import {
  ArcElement,
  Chart as ChartJS,
  ChartData,
  ChartOptions,
  Legend,
  Tooltip,
} from "chart.js";
import { Doughnut } from "react-chartjs-2";

ChartJS.register(ArcElement, Tooltip, Legend);

export const options = {
  responsive: true,
  onHover: (event, element) => {
    const canvas: any = event.native?.target;
    canvas.style.cursor = element[0] ? "pointer" : "default";
  },
  plugins: {
    legend: {
      display: false,
    },
  },
  scales: {
    x: {
      display: false,
    },
    y: {
      display: false,
    },
  },
  cutout: "70%",
} as ChartOptions<"doughnut">;

type Props = HTMLProps<HTMLDivElement> & {
  chartData: ChartData<"doughnut", number[], string>;
  label: string[];
  loading: boolean;
};

const PieChart: React.FC<Props> = ({ chartData, label, loading }) => {
  const dataLabels = [
    { color: "#343C6A" },
    { color: "#FC7900" },
    { color: "#1814F3" },
  ];
  return (
    <div className="bg-white p-6 rounded-2xl">
      <h1 className="text-lg font-medium text-primary-2">
        Grafik Jumlah Cluster
      </h1>
      {loading ? (
        <div className="flex justify-center items-center h-full">
          <h1 className="text-2xl font-bold">Loading...</h1>
        </div>
      ) : (
        <>
          <div className="flex justify-center mt-4">
            <div className="w-[90%]">
              <Doughnut data={chartData} options={options} />
            </div>
          </div>
          <div className="grid grid-cols-3 gap-4 mt-8">
            {label.map((item, key) => (
              <div key={key} className="flex items-center space-x-2">
                <div
                  style={{ backgroundColor: dataLabels[key].color }}
                  className="w-4 h-4 rounded-full"
                />
                <h1 className="capitalize">{item}</h1>
              </div>
            ))}
          </div>
        </>
      )}
    </div>
  );
};

export default PieChart;
