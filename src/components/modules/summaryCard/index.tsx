"use client";

import React from "react";
import ICONS from "@/config/icons";

type Props = {
  label: string;
  data: number;
  color: "blue" | "yellow" | "pink" | "cyn";
};

const SummaryCard: React.FC<Props> = ({ label, data, color }) => {
  const isBlue = color === "blue";
  const isYellow = color === "yellow";
  const isPink = color === "pink";
  const isCyn = color === "cyn";

  return (
    <div className="flex items-center space-x-4 bg-white p-4 rounded-lg shadow">
      {isBlue ? (
        <div className="bg-primary-4 p-3 rounded-full">
          <ICONS.NoteBlue />
        </div>
      ) : null}
      {isYellow ? (
        <div className="bg-yellow-2 p-3 rounded-full">
          <ICONS.NoteYellow />
        </div>
      ) : null}
      {isPink ? (
        <div className="bg-pink-2 p-3 rounded-full">
          <ICONS.NotePink />
        </div>
      ) : null}
      {isCyn ? (
        <div className="bg-cyn-2 p-3 rounded-full">
          <ICONS.NoteCyn />
        </div>
      ) : null}
      <div className="space-y-1">
        <p className="text-sm text-primary-2">{label}</p>
        <p className="text-xl font-semibold">{data}</p>
      </div>
    </div>
  );
};

export default SummaryCard;
