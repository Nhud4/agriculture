"use client";

import React from "react";
import Select from "react-select";
import { SingleValue } from "react-select";

type Props = {
  defaultValue?: { value: string | number | boolean; label: string };
  name: string;
  options: { value: string | number | boolean; label: string }[];
  label?: string;
  placeholder?: string;
  isClearable?: boolean;
  value?: { value: string | number | boolean; label: string }[];
  onChange: (
    values: SingleValue<{ value: string | number | boolean; label: string }>
  ) => void;
  isDisabled?: boolean;
  maxMenuHeight?: number;
};

const Dropdown: React.FC<Props> = ({
  defaultValue,
  name,
  options,
  label,
  placeholder,
  isClearable = true,
  onChange,
  isDisabled,
  maxMenuHeight,
}) => {
  return (
    <div>
      {label ? <h1 className="text-base font-medium pb-2">{label}</h1> : null}
      <Select
        maxMenuHeight={maxMenuHeight}
        isDisabled={isDisabled}
        className="capitalize"
        options={options}
        name={name}
        defaultValue={defaultValue}
        placeholder={placeholder}
        isClearable={isClearable}
        onChange={(choice) => onChange(choice)}
        styles={{
          menu: (rest) => ({ ...rest, zIndex: 9999 }),
          control: (rest) => ({ ...rest, cursor: "pointer", minWidth: 175 }),
          option: (rest) => ({ ...rest, cursor: "pointer" }),
        }}
      />
    </div>
  );
};

export default Dropdown;
