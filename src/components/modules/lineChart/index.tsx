"use client";

import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  ChartOptions,
  ChartData,
} from "chart.js";
import { Line } from "react-chartjs-2";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

type Props = {
  data: ChartData<"line", number[], string>;
  loading: boolean;
};

const LineChart: React.FC<Props> = ({ data, loading }) => {
  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: "top" as const,
      },
    },
    layout: {
      padding: {
        top: 20,
      },
    },
    scales: {
      y: {
        grid: {
          borderDash: [15],
        },
      },
      x: {
        grid: {
          display: false,
        },
      },
    },
  } as ChartOptions<"line">;

  return (
    <div className="bg-white p-4 rounded-lg">
      <h1 className="text-lg font-medium text-primary-2">
        Grafik Pertanian Perkecamatan
      </h1>
      {loading ? (
        <div className="flex justify-center items-center h-full">
          <h1 className="text-2xl font-bold">Loading...</h1>
        </div>
      ) : (
        <Line options={options} data={data} />
      )}
    </div>
  );
};

export default LineChart;
