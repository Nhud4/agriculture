"use client";

import React, { useState } from "react";
import IMAGES from "@/config/images";
import Image from "next/image";
import Button from "../button";
import Spinner from "@/components/elements/spinner";

type Props = {
  onClose: () => void;
  onConfirm: () => void;
};

export default function Questions({ onClose, onConfirm }: Props) {
  const [loading, setLoading] = useState(false);

  return (
    <div>
      <div className="flex flex-col justify-center items-center">
        <Image alt="delete" src={IMAGES.Delete} />
        <div className="pt-4">
          <p>Apakah anda yakin ingin menghapus</p>
          <p>secara permanen data yang dipilih?</p>
        </div>
      </div>
      <div className="flex justify-center items-center space-x-4 pt-8">
        <Button
          type="button"
          className="!bg-white border border-primary !text-primary !w-32"
          onClick={onClose}
        >
          Batal
        </Button>
        <Button
          type="submit"
          className="!w-32"
          onClick={() => {
            setLoading(true);
            onConfirm();
          }}
        >
          {loading ? <Spinner /> : "Konfirmasi"}
        </Button>
      </div>
    </div>
  );
}
