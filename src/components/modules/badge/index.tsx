"use client";

import React from "react";

type Props = {
  children: React.ReactNode;
  className?: string;
};

export default function Badge({ children, className }: Props) {
  return (
    <div className={[className, "py-2 px-4 rounded-lg capitalize"].join(" ")}>
      {children}
    </div>
  );
}
