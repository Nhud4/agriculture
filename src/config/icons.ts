import Home from "../../public/assets/icons/home.svg";
import User from "../../public/assets/icons/user.svg";
import Box from "../../public/assets/icons/box.svg";
import Bag from "../../public/assets/icons/bag-seedling.svg";
import Sort from "../../public/assets/icons/apps-sort.svg";
import Report from "../../public/assets/icons/document-signed.svg";
import NoteBlue from "../../public/assets/icons/document.svg";
import Dote from "../../public/assets/icons/dote.svg";
import Search from "../../public/assets/icons/search-normal.svg";
import Arrow from "../../public/assets/icons/arrow.svg";
import Close from "../../public/assets/icons/close-circle.svg";
import Edit from "../../public/assets/icons/edit-.svg";
import Eyes from "../../public/assets/icons/eye.svg";
import Plus from "../../public/assets/icons/plus.svg";
import HomeActive from "../../public/assets/icons/home-active.svg";
import UserActive from "../../public/assets/icons/user-active.svg";
import BoxActive from "../../public/assets/icons/box-active.svg";
import BagActive from "../../public/assets/icons/bag-active.svg";
import SortActive from "../../public/assets/icons/apps-active.svg";
import ReportActive from "../../public/assets/icons/document-active.svg";
import NotePink from "../../public/assets/icons/document-2.svg";
import NoteYellow from "../../public/assets/icons/document-1.svg";
import NoteCyn from "../../public/assets/icons/document-3.svg";
import Cross from "../../public/assets/icons/cross.svg";
import Refresh from "../../public/assets/icons/refresh.svg";

const ICONS = {
  Home,
  User,
  Box,
  Bag,
  Sort,
  Report,
  NoteBlue,
  Dote,
  Search,
  Arrow,
  Close,
  Edit,
  Eyes,
  Plus,
  HomeActive,
  UserActive,
  BoxActive,
  BagActive,
  SortActive,
  ReportActive,
  NotePink,
  NoteYellow,
  NoteCyn,
  Cross,
  Refresh,
};

export default ICONS;
