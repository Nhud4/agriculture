import ICONS from "./icons";

const ROUTES = [
  {
    path: "/dashboard",
    label: "Dashboard",
    icon: <ICONS.Home />,
    activeIcon: <ICONS.HomeActive />,
  },
  {
    path: "/pertanian",
    label: "Pertanian",
    icon: <ICONS.Bag />,
    activeIcon: <ICONS.BagActive />,
  },
  {
    path: "/report",
    label: "Pelaporan",
    icon: <ICONS.Report />,
    activeIcon: <ICONS.ReportActive />,
  },
  {
    path: "/setting",
    label: "Pusat Data",
    icon: <ICONS.Box />,
    activeIcon: <ICONS.BoxActive />,
  },
  {
    path: "/user",
    label: "Informasi Akun",
    icon: <ICONS.User />,
    activeIcon: <ICONS.UserActive />,
  },
];

export default ROUTES;
