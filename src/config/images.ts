import Logo from "../../public/assets/image/logo-2.png";
import FullLogo from "../../public/assets/image/logo-3.png";
import Profile from "../../public/assets/image/profile.png";
import EmptyData from "../../public/assets/image/empty.jpg";
import LoadingData from "../../public/assets/image/loading.jpg";
import Delete from "../../public/assets/image/question.png";

const IMAGES = {
  Logo,
  FullLogo,
  Profile,
  EmptyData,
  LoadingData,
  Delete,
};

export default IMAGES;
